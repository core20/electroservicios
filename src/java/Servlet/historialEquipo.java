/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Controlador.HistorialEquipoJpaController;
import Controlador.exceptions.PreexistingEntityException;
import Controlador.exceptions.RollbackFailureException;
import Entidades.Entrada;
import Entidades.EstadoActualizacion;
import Entidades.HistorialEquipo;
import Entidades.HistorialEquipoFacade;
import Entidades.Usuario;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

/**
 *
 * @author jorge
 */
@WebServlet(name = "historialEquipo", urlPatterns = {"/historialEquipo"})
public class historialEquipo extends HttpServlet {
    @EJB
    private HistorialEquipoFacade historialFacade;
    
    @PersistenceUnit 
    EntityManagerFactory emf;
    EntityManager em;
    @Resource
    UserTransaction utx;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            if(request.getParameter("find") != null){
                HistorialEquipo historialEquipo = new HistorialEquipo();
                //historialFacade.fin
            }else if(request.getParameter("agregar") != null){
                Gson gson = new Gson();
                HistorialEquipo historialEquipo = gson.fromJson(request.getParameter("json"), HistorialEquipo.class);
                //El constructor no solicita las FK, por lo que se setean después
                Entrada entrada = new Entrada( Long.valueOf( request.getParameter("entradaId").toString() ) );
                historialEquipo.setEntradaId(entrada);
                
                EstadoActualizacion estado = new EstadoActualizacion( Long.valueOf( request.getParameter("estadoId").toString() ) );
                historialEquipo.setEstadoActualizacionId(estado);
                
                Usuario usuario = new Usuario(Long.valueOf(1));
                historialEquipo.setUsuarioId(usuario);
                
                String salida = "";
                Boolean noError = true;
                
                HistorialEquipoJpaController hejc = new HistorialEquipoJpaController(utx, emf);
                
                try {
                    hejc.create(historialEquipo);
                } catch (PreexistingEntityException ex) {
                    Logger.getLogger(historialEquipo.class.getName()).log(Level.SEVERE, null, ex);
                    salida += "exists";
                    noError = false;
                } catch (RollbackFailureException ex) {
                    Logger.getLogger(historialEquipo.class.getName()).log(Level.SEVERE, null, ex);
                    salida += "error1";
                    noError = false;
                } catch (Exception ex) {
                    Logger.getLogger(historialEquipo.class.getName()).log(Level.SEVERE, null, ex);
                    salida += "error2";
                    noError = false;
                }
                
                out.println("{\"noError\":"+noError+"}");
            }
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
