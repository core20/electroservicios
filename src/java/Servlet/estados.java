/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Controlador.EstadoActualizacionFacade;
import Controlador.EstadoActualizacionJpaController;
import Controlador.exceptions.IllegalOrphanException;
import Controlador.exceptions.NonexistentEntityException;
import Controlador.exceptions.RollbackFailureException;
import Entidades.EstadoActualizacion;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

/**
 *
 * @author ale
 */
@WebServlet(name = "estados", urlPatterns = {"/estados"})
public class estados extends HttpServlet {
    @EJB
    private EstadoActualizacionFacade estadoFacade;
    
    @PersistenceUnit 
    EntityManagerFactory emf;
    EntityManager em;
    @Resource
    UserTransaction utx;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");//Agregado para capturar carácteres especiales correctamente
        response.setContentType("application/json;charset=UTF-8");//EDITADO DE ORIGINAL PARA JSON
        response.setHeader("Content-Disposition", "inline");//AGREGADO PARA JSON
        PrintWriter out = response.getWriter();
        try {
            /*Innecesario para JpaController*/
            /*em = emf.createEntityManager();
            try {
                utx.begin();
            } catch (NotSupportedException ex) {
                Logger.getLogger(empleados.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SystemException ex) {
                Logger.getLogger(empleados.class.getName()).log(Level.SEVERE, null, ex);
            }*/
            
            EstadoActualizacionJpaController ejc = new EstadoActualizacionJpaController(utx, emf);
            
            if(request.getParameter("find") != null){
                //Gson gson = new Gson();
                Gson gson = new GsonBuilder().setPrettyPrinting().create();//Impresión con espacios y saltos de línea (para humanos)
                
                int start = ((request.getParameter("start") != null)?Integer.parseInt(request.getParameter("start").toString()):0);
                List estados = ejc.findEstadoActualizacionEntities(10,start);
                EstadoActualizacion estado;
                ArrayList listaEstados = new ArrayList();

                for (int i = 0; i < estados.size(); i++) {
                    estado = (EstadoActualizacion) estados.get(i);
                    listaEstados.add( new EstadoActualizacion( estado.getActualizacionId(),estado.getNombre(),estado.getActualizacionTipo() ) );
                }

                /*Salida de Array List en formato JSON*/
                out.print(gson.toJson(listaEstados));
                
            }else if(request.getParameter("encontrar") != null){
                Gson gson = new GsonBuilder().setPrettyPrinting().create();//Impresión con espacios y saltos de línea (para humanos)
                EstadoActualizacion estado = ejc.findEstadoActualizacion(Long.valueOf( request.getParameter("id_estado").toString() ) );
                
                out.print(gson.toJson( new EstadoActualizacion( estado.getActualizacionId(), estado.getNombre(), estado.getActualizacionTipo() )));
            }else if(request.getParameter("agregar") != null){
                Gson gson = new Gson();
                EstadoActualizacion estado = gson.fromJson(request.getParameter("json"), EstadoActualizacion.class);
                
                String salida = "";
                Boolean noError = true;
                
                estadoFacade.create(estado);
                /*try {
                    ejc.create( empleado );
                } catch (PreexistingEntityException ex) {
                    Logger.getLogger(empleados.class.getName()).log(Level.SEVERE, null, ex);
                    salida += "exists";
                    noError = false;
                } catch (RollbackFailureException ex) {
                    Logger.getLogger(empleados.class.getName()).log(Level.SEVERE, null, ex);
                    //System.out.println("Error");
                    salida += "error1";
                    noError = false;
                } catch (Exception ex) {
                    Logger.getLogger(empleados.class.getName()).log(Level.SEVERE, null, ex);
                    salida += "error2";
                    noError = false;
                }*/
                
                out.println("{\"noError\":"+noError+"}");
            }else if(request.getParameter("editar") != null){
                Gson gson = new Gson();
                EstadoActualizacion estado = gson.fromJson(request.getParameter("json"), EstadoActualizacion.class);
                
                String salida = "";
                Boolean noError = true;
                
                estadoFacade.edit(estado);
                /*try {
                    ejc.create( empleado );
                } catch (PreexistingEntityException ex) {
                    Logger.getLogger(empleados.class.getName()).log(Level.SEVERE, null, ex);
                    salida += "exists";
                    noError = false;
                } catch (RollbackFailureException ex) {
                    Logger.getLogger(empleados.class.getName()).log(Level.SEVERE, null, ex);
                    //System.out.println("Error");
                    salida += "error1";
                    noError = false;
                } catch (Exception ex) {
                    Logger.getLogger(empleados.class.getName()).log(Level.SEVERE, null, ex);
                    salida += "error2";
                    noError = false;
                }*/
                
                out.println("{\"noError\":"+noError+"}");
            }else if(request.getParameter("delete") != null){
                Gson gson = new Gson();
                
                String salida = "";
                Boolean noError = true;
                try {
                    ejc.destroy( Long.valueOf( request.getParameter("estado_id").toString() ) );
                } catch (IllegalOrphanException ex) {
                    Logger.getLogger(estados.class.getName()).log(Level.SEVERE, null, ex);
                    noError = false;
                } catch (NonexistentEntityException ex) {
                    Logger.getLogger(estados.class.getName()).log(Level.SEVERE, null, ex);
                    noError = false;
                } catch (RollbackFailureException ex) {
                    Logger.getLogger(estados.class.getName()).log(Level.SEVERE, null, ex);
                    noError = false;
                } catch (Exception ex) {
                    Logger.getLogger(estados.class.getName()).log(Level.SEVERE, null, ex);
                    noError = false;
                }
                
                out.println("{\"noError\":"+noError+"}");
            }
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
