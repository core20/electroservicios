/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Controlador.EmpleadoFacade;
import Controlador.EmpleadoJpaController;
import Controlador.exceptions.IllegalOrphanException;
import Controlador.exceptions.NonexistentEntityException;
import Controlador.exceptions.RollbackFailureException;
import Entidades.Empleado;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

/**
 *
 * @author jorge
 */
@WebServlet(name = "empleados", urlPatterns = {"/empleados"})
public class empleados extends HttpServlet {
    @EJB
    private EmpleadoFacade empleadoFacade;
    
    @PersistenceUnit 
    EntityManagerFactory emf;
    EntityManager em;
    @Resource
    UserTransaction utx;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");//Agregado para capturar carácteres especiales correctamente
        response.setContentType("application/json;charset=UTF-8");//EDITADO DE ORIGINAL PARA JSON
        response.setHeader("Content-Disposition", "inline");//AGREGADO PARA JSON
        PrintWriter out = response.getWriter();
        try {
            /*Innecesario para JpaController*/
            /*em = emf.createEntityManager();
            try {
                utx.begin();
            } catch (NotSupportedException ex) {
                Logger.getLogger(empleados.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SystemException ex) {
                Logger.getLogger(empleados.class.getName()).log(Level.SEVERE, null, ex);
            }*/
            
            EmpleadoJpaController ejc = new EmpleadoJpaController(utx, emf);
            
            if(request.getParameter("find") != null){
                //Gson gson = new Gson();
                Gson gson = new GsonBuilder().setPrettyPrinting().create();//Impresión con espacios y saltos de línea (para humanos)
                
                int start = ((request.getParameter("start") != null)?Integer.parseInt(request.getParameter("start").toString()):0);
                List empleados = ejc.findEmpleadoEntities(10,start);
                Empleado empleado;
                ArrayList listaEmpleados = new ArrayList();

                for (int i = 0; i < empleados.size(); i++) {
                    empleado = (Empleado) empleados.get(i);
                    listaEmpleados.add( new Empleado( empleado.getRut(),empleado.getRutVerif(),empleado.getNombres(),empleado.getPrimerApellido(),empleado.getSegundoApellido() ) );
                }

                /*Salida de Array List en formato JSON*/
                out.print(gson.toJson(listaEmpleados));
                
            }else if(request.getParameter("encontrar") != null){
                Gson gson = new GsonBuilder().setPrettyPrinting().create();//Impresión con espacios y saltos de línea (para humanos)
                Empleado empleado = ejc.findEmpleado( Long.valueOf( request.getParameter("rut").toString() ) );
                
                out.print(gson.toJson( new Empleado( empleado.getRut(), empleado.getRutVerif(), empleado.getNombres(), empleado.getPrimerApellido(), empleado.getSegundoApellido() ) ));
            }else if(request.getParameter("agregar") != null){
                Gson gson = new Gson();
                Empleado empleado = gson.fromJson(request.getParameter("json"), Empleado.class);
                //Empleado empleado = gson.fromJson( new String(request.getParameter("json").getBytes("ISO-8859-1"),"UTF-8") , Empleado.class);//Cambiado por conflicto con carácteres UTF-8//Cambiado por segunda solución modificando request.setCharacterEncoding("UTF-8");
                
                String salida = "";
                Boolean noError = true;
                
                empleadoFacade.create(empleado);
                /*try {
                    ejc.create( empleado );
                } catch (PreexistingEntityException ex) {
                    Logger.getLogger(empleados.class.getName()).log(Level.SEVERE, null, ex);
                    salida += "exists";
                    noError = false;
                } catch (RollbackFailureException ex) {
                    Logger.getLogger(empleados.class.getName()).log(Level.SEVERE, null, ex);
                    //System.out.println("Error");
                    salida += "error1";
                    noError = false;
                } catch (Exception ex) {
                    Logger.getLogger(empleados.class.getName()).log(Level.SEVERE, null, ex);
                    salida += "error2";
                    noError = false;
                }*/
                
                out.println("{\"noError\":"+noError+"}");
            }else if(request.getParameter("editar") != null){
                Gson gson = new Gson();
                Empleado empleado = gson.fromJson(request.getParameter("json"), Empleado.class);
                //Empleado empleado = gson.fromJson( new String(request.getParameter("json").getBytes("ISO-8859-1"),"UTF-8") , Empleado.class);//Cambiado por conflicto con carácteres UTF-8//Cambiado por segunda solución modificando request.setCharacterEncoding("UTF-8");
                
                String salida = "";
                Boolean noError = true;
                
                empleadoFacade.edit(empleado);
                /*try {
                    ejc.create( empleado );
                } catch (PreexistingEntityException ex) {
                    Logger.getLogger(empleados.class.getName()).log(Level.SEVERE, null, ex);
                    salida += "exists";
                    noError = false;
                } catch (RollbackFailureException ex) {
                    Logger.getLogger(empleados.class.getName()).log(Level.SEVERE, null, ex);
                    //System.out.println("Error");
                    salida += "error1";
                    noError = false;
                } catch (Exception ex) {
                    Logger.getLogger(empleados.class.getName()).log(Level.SEVERE, null, ex);
                    salida += "error2";
                    noError = false;
                }*/
                
                out.println("{\"noError\":"+noError+"}");
            }else if(request.getParameter("delete") != null){
                Gson gson = new Gson();
                
                String salida = "";
                Boolean noError = true;
                try {
                    ejc.destroy( Long.valueOf( request.getParameter("rut").toString() ) );
                } catch (IllegalOrphanException ex) {
                    Logger.getLogger(empleados.class.getName()).log(Level.SEVERE, null, ex);
                    noError = false;
                } catch (NonexistentEntityException ex) {
                    Logger.getLogger(empleados.class.getName()).log(Level.SEVERE, null, ex);
                    noError = false;
                } catch (RollbackFailureException ex) {
                    Logger.getLogger(empleados.class.getName()).log(Level.SEVERE, null, ex);
                    noError = false;
                } catch (Exception ex) {
                    Logger.getLogger(empleados.class.getName()).log(Level.SEVERE, null, ex);
                    noError = false;
                }
                
                out.println("{\"noError\":"+noError+"}");
            }
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
