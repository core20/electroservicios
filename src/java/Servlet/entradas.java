/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Controlador.EntradaFacade;
import Controlador.EntradaJpaController;
import Entidades.Cliente;
import Entidades.Entrada;
import Entidades.HistorialEquipo;
import Entidades.Marca;
import Entidades.TipoEquipo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

/**
 *
 * @author jorge
 */
@WebServlet(name = "entradas", urlPatterns = {"/entradas"})
public class entradas extends HttpServlet {
    @EJB
    private EntradaFacade entradaFacade;
    
    @PersistenceUnit 
    EntityManagerFactory emf;
    EntityManager em;
    @Resource
    UserTransaction utx;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");//Agregado para capturar carácteres especiales correctamente
        response.setContentType("application/json;charset=UTF-8");//EDITADO DE ORIGINAL PARA JSON
        response.setHeader("Content-Disposition", "inline");//AGREGADO PARA JSON
        PrintWriter out = response.getWriter();
        try {
            EntradaJpaController ejc = new EntradaJpaController(utx, emf);
            
            if(request.getParameter("find") != null){
                //Gson gson = new Gson();
                Gson gson = new GsonBuilder().setPrettyPrinting().create();//Impresión con espacios y saltos de línea (para humanos)
                
                int start = ((request.getParameter("start") != null)?Integer.parseInt(request.getParameter("start").toString()):0);
                List entradas = ejc.findEntradaEntities(10,start);
                Entrada entrada;
                ArrayList listaEntradas = new ArrayList();

                for (int i = 0; i < entradas.size(); i++) {
                    entrada = (Entrada) entradas.get(i);
                    listaEntradas.add( new Entrada( entrada.getEntradaId(), entrada.getSerie(), entrada.getModelo(), entrada.getFechaEntrada(), entrada.getFechaPrevistaEntrega(), entrada.getComentario(), entrada.getAccesorios() ) );
                }

                /*Salida de Array List en formato JSON*/
                out.print(gson.toJson(listaEntradas));
                
            }else if(request.getParameter("encontrar") != null){
                Gson gson = new GsonBuilder().setPrettyPrinting().create();//Impresión con espacios y saltos de línea (para humanos)
                Entrada entrada = ejc.findEntrada( Long.valueOf( request.getParameter("id").toString() ) );
                
                out.print(gson.toJson( new Entrada(entrada.getEntradaId(), entrada.getSerie(), entrada.getModelo(), entrada.getFechaEntrada(), entrada.getFechaPrevistaEntrega(), entrada.getComentario(), entrada.getAccesorios()) ));
            }else if(request.getParameter("agregar") != null){
                Gson gson = new Gson();
                Entrada entrada = gson.fromJson(request.getParameter("json"), Entrada.class);
                entrada.setEntradaId(Long.valueOf("0"));
                
                //El constructor no solicita las FK, por lo que se setean después
                Cliente cliente = new Cliente( Long.valueOf( request.getParameter("rutCliente").toString() ) );
                entrada.setRutCliente( cliente );
                
                TipoEquipo tipoEquipo = new TipoEquipo( Long.valueOf( request.getParameter("tipoEquipo").toString() ) );
                entrada.setTipoEquipoId( tipoEquipo );
                
                Marca marca = new Marca( Long.valueOf( request.getParameter("marca").toString() ) );
                entrada.setMarcaId( marca );
                
                String salida = "";
                Boolean noError = true;
                
                entradaFacade.create(entrada);
                
                //Agregar la entrada al historial
                /*{
                    HistorialEquipo historialEquipo = new HistorialEquipo(
                            Long.valueOf(1),
                            Long.valueOf(1),
                            "Recibido",
                            Long.valueOf(0)
                            );
                    
                    historialEquipo.setEntradaId(entrada);
                    
                    EstadoActualizacion estado = new EstadoActualizacion( Long.valueOf(1) );
                    historialEquipo.setEstadoActualizacionId(estado);

                    Usuario usuario = new Usuario(Long.valueOf(1));
                    historialEquipo.setUsuarioId(usuario);
                    
                    HistorialEquipoJpaController hejc = new HistorialEquipoJpaController(utx, emf);
                    try {
                        hejc.create(historialEquipo);
                    } catch (PreexistingEntityException ex) {
                        Logger.getLogger(entradas.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (RollbackFailureException ex) {
                        Logger.getLogger(entradas.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (Exception ex) {
                        Logger.getLogger(entradas.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }*/
                
                /*try {
                    ejc.create( entrada );
                } catch (PreexistingEntityException ex) {
                    Logger.getLogger(entradas.class.getName()).log(Level.SEVERE, null, ex);
                    salida += "exists";
                    noError = false;
                } catch (RollbackFailureException ex) {
                    Logger.getLogger(entradas.class.getName()).log(Level.SEVERE, null, ex);
                    salida += "error1";
                    noError = false;
                } catch (Exception ex) {
                    Logger.getLogger(entradas.class.getName()).log(Level.SEVERE, null, ex);
                    salida += "error2";
                    noError = false;
                }*/
                
                out.println("{\"noError\":"+noError+"}");
            }else if(request.getParameter("historial") != null){
                Gson gson = new GsonBuilder().setPrettyPrinting().create();//Impresión con espacios y saltos de línea (para humanos)
                Entrada entrada = ejc.findEntrada( Long.valueOf( request.getParameter("id").toString() ) );
                
                HistorialEquipo historialEquipo;
                ArrayList listaHistorialEquipo = new ArrayList(entrada.getHistorialEquipoCollection());
                ArrayList salida = new ArrayList();
                
                for (int i = 0; i < listaHistorialEquipo.size(); i++) {
                    historialEquipo = (HistorialEquipo) listaHistorialEquipo.get(i);
                    salida.add( new HistorialEquipo(historialEquipo.getHistorialEquipoId(), historialEquipo.getFecha(), historialEquipo.getDetalle(), historialEquipo.getValor()) );
                }
                
                out.print(gson.toJson( salida ));
            }
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
