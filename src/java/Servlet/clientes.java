/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Controlador.ClienteFacade;
import Controlador.ClienteJpaController;
import Controlador.exceptions.IllegalOrphanException;
import Controlador.exceptions.NonexistentEntityException;
import Controlador.exceptions.PreexistingEntityException;
import Controlador.exceptions.RollbackFailureException;
import Entidades.Cliente;
import Entidades.Comuna;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

/**
 *
 * @author jorge
 */
@WebServlet(name = "clientes", urlPatterns = {"/clientes"})
public class clientes extends HttpServlet {
    @EJB
    private ClienteFacade clienteFacade;
    
    @PersistenceUnit 
    EntityManagerFactory emf;
    EntityManager em;
    @Resource
    UserTransaction utx;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");//Agregado para capturar carácteres especiales correctamente
        response.setContentType("application/json;charset=UTF-8");//EDITADO DE ORIGINAL PARA JSON
        response.setHeader("Content-Disposition", "inline");//AGREGADO PARA JSON
        PrintWriter out = response.getWriter();
        try {
            ClienteJpaController cjc = new ClienteJpaController(utx, emf);
            
            if(request.getParameter("find") != null){
                //Gson gson = new Gson();
                Gson gson = new GsonBuilder().setPrettyPrinting().create();//Impresión con espacios y saltos de línea (para humanos)
                int start = ((request.getParameter("start") != null)?Integer.parseInt(request.getParameter("start").toString()):0);
                List clientes = cjc.findClienteEntities(10,start);
                Cliente cliente;
                ArrayList listaClientes = new ArrayList();

                for (int i = 0; i < clientes.size(); i++) {
                    cliente = (Cliente) clientes.get(i);
                    listaClientes.add( new Cliente( cliente.getRut(), cliente.getRutVerif(), cliente.getNombres(), cliente.getPrimerApellido(), cliente.getSegundoApellido(), cliente.getDireccion(), cliente.getFono1(), cliente.getFono2(), cliente.getEmail(), cliente.getClienteTipo() ) );
                }

                /*Salida de Array List en formato JSON*/
                out.print(gson.toJson(listaClientes));
            }else if(request.getParameter("encontrar") != null){
                Gson gson = new GsonBuilder().setPrettyPrinting().create();//Impresión con espacios y saltos de línea (para humanos)
                Cliente cliente = cjc.findCliente( Long.valueOf( request.getParameter("rut").toString() ) );
                
                out.print(gson.toJson( new Cliente( cliente.getRut(), cliente.getRutVerif(), cliente.getNombres(), cliente.getPrimerApellido(), cliente.getSegundoApellido(), cliente.getDireccion(), cliente.getFono1(), cliente.getFono2(), cliente.getEmail(), cliente.getClienteTipo() ) ));
            }else if(request.getParameter("agregar") != null){
                Gson gson = new Gson();
                Cliente cliente = gson.fromJson(request.getParameter("json"), Cliente.class);
                //El constructor no solicita las FK, por lo que se setean después
                Comuna comuna = new Comuna( Integer.parseInt( request.getParameter("codigoComuna").toString() ) );
                cliente.setCodigoComuna( comuna );
                
                String salida = "";
                Boolean noError = true;
                
                try {
                    cjc.create( cliente );
                } catch (PreexistingEntityException ex) {
                    Logger.getLogger(clientes.class.getName()).log(Level.SEVERE, null, ex);
                    salida += "exists";
                    noError = false;
                } catch (RollbackFailureException ex) {
                    Logger.getLogger(clientes.class.getName()).log(Level.SEVERE, null, ex);
                    salida += "error1";
                    noError = false;
                } catch (Exception ex) {
                    Logger.getLogger(clientes.class.getName()).log(Level.SEVERE, null, ex);
                    salida += "error2";
                    noError = false;
                }
                
                out.println("{\"noError\":"+noError+"}");
            }else if(request.getParameter("editar") != null){
                Gson gson = new Gson();
                Cliente cliente = gson.fromJson(request.getParameter("json"), Cliente.class);
                //El constructor no solicita las FK, por lo que se setean después
                Comuna comuna = new Comuna( Integer.parseInt( request.getParameter("codigoComuna").toString() ) );
                cliente.setCodigoComuna( comuna );
                
                String salida = "";
                Boolean noError = true;
                clienteFacade.edit( cliente );
                
                out.println("{\"noError\":"+noError+"}");
            }else if(request.getParameter("delete") != null){
                Gson gson = new Gson();
                
                String salida = "";
                Boolean noError = true;
                
                try {
                    cjc.destroy(Long.valueOf( request.getParameter("rut").toString() ) );
                } catch (IllegalOrphanException ex) {
                    Logger.getLogger(clientes.class.getName()).log(Level.SEVERE, null, ex);
                    noError = false;
                } catch (NonexistentEntityException ex) {
                    Logger.getLogger(clientes.class.getName()).log(Level.SEVERE, null, ex);
                    noError = false;
                } catch (RollbackFailureException ex) {
                    Logger.getLogger(clientes.class.getName()).log(Level.SEVERE, null, ex);
                    noError = false;
                } catch (Exception ex) {
                    Logger.getLogger(clientes.class.getName()).log(Level.SEVERE, null, ex);
                    noError = false;
                }
                
                out.println("{\"noError\":"+noError+"}");
            }
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
