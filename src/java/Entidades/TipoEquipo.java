/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jorge
 */
@Entity
@Table(name = "TIPO_EQUIPO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoEquipo.findAll", query = "SELECT t FROM TipoEquipo t"),
    @NamedQuery(name = "TipoEquipo.findByTipoId", query = "SELECT t FROM TipoEquipo t WHERE t.tipoId = :tipoId"),
    @NamedQuery(name = "TipoEquipo.findByNombre", query = "SELECT t FROM TipoEquipo t WHERE t.nombre = :nombre"),
    @NamedQuery(name = "TipoEquipo.findByCostoRevision", query = "SELECT t FROM TipoEquipo t WHERE t.costoRevision = :costoRevision")})
public class TipoEquipo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "TIPO_ID")
    private Long tipoId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NOMBRE")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "COSTO_REVISION")
    private int costoRevision;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipoEquipoId")
    private Collection<Entrada> entradaCollection;

    public TipoEquipo() {
    }

    public TipoEquipo(Long tipoId) {
        this.tipoId = tipoId;
    }

    public TipoEquipo(Long tipoId, String nombre, int costoRevision) {
        this.tipoId = tipoId;
        this.nombre = nombre;
        this.costoRevision = costoRevision;
    }

    public Long getTipoId() {
        return tipoId;
    }

    public void setTipoId(Long tipoId) {
        this.tipoId = tipoId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCostoRevision() {
        return costoRevision;
    }

    public void setCostoRevision(int costoRevision) {
        this.costoRevision = costoRevision;
    }

    @XmlTransient
    public Collection<Entrada> getEntradaCollection() {
        return entradaCollection;
    }

    public void setEntradaCollection(Collection<Entrada> entradaCollection) {
        this.entradaCollection = entradaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tipoId != null ? tipoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoEquipo)) {
            return false;
        }
        TipoEquipo other = (TipoEquipo) object;
        if ((this.tipoId == null && other.tipoId != null) || (this.tipoId != null && !this.tipoId.equals(other.tipoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entidades.TipoEquipo[ tipoId=" + tipoId + " ]";
    }
    
}
