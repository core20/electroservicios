/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jorge
 */
@Entity
@Table(name = "PROVINCIA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Provincia.findAll", query = "SELECT p FROM Provincia p"),
    @NamedQuery(name = "Provincia.findByCodigoProvincia", query = "SELECT p FROM Provincia p WHERE p.codigoProvincia = :codigoProvincia"),
    @NamedQuery(name = "Provincia.findByNombre", query = "SELECT p FROM Provincia p WHERE p.nombre = :nombre"),
    /*Búsqueda agregada*/
    @NamedQuery(name = "Provincia.findByCodigoRegion", query = "SELECT p FROM Provincia p WHERE p.codigoRegion = :codigoRegion")})
public class Provincia implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CODIGO_PROVINCIA")
    private Short codigoProvincia;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NOMBRE")
    private String nombre;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codigoProvincia")
    private Collection<Comuna> comunaCollection;
    @JoinColumn(name = "CODIGO_REGION", referencedColumnName = "CODIGO_REGION")
    @ManyToOne(optional = false)
    private Region codigoRegion;

    public Provincia() {
    }

    public Provincia(Short codigoProvincia) {
        this.codigoProvincia = codigoProvincia;
    }

    public Provincia(Short codigoProvincia, String nombre) {
        this.codigoProvincia = codigoProvincia;
        this.nombre = nombre;
    }

    public Short getCodigoProvincia() {
        return codigoProvincia;
    }

    public void setCodigoProvincia(Short codigoProvincia) {
        this.codigoProvincia = codigoProvincia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @XmlTransient
    public Collection<Comuna> getComunaCollection() {
        return comunaCollection;
    }

    public void setComunaCollection(Collection<Comuna> comunaCollection) {
        this.comunaCollection = comunaCollection;
    }

    public Region getCodigoRegion() {
        return codigoRegion;
    }

    public void setCodigoRegion(Region codigoRegion) {
        this.codigoRegion = codigoRegion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoProvincia != null ? codigoProvincia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Provincia)) {
            return false;
        }
        Provincia other = (Provincia) object;
        if ((this.codigoProvincia == null && other.codigoProvincia != null) || (this.codigoProvincia != null && !this.codigoProvincia.equals(other.codigoProvincia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entidades.Provincia[ codigoProvincia=" + codigoProvincia + " ]";
    }
    
}
