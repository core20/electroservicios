/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jorge
 */
@Entity
@Table(name = "HISTORIAL_EQUIPO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HistorialEquipo.findAll", query = "SELECT h FROM HistorialEquipo h"),
    @NamedQuery(name = "HistorialEquipo.findByHistorialEquipoId", query = "SELECT h FROM HistorialEquipo h WHERE h.historialEquipoId = :historialEquipoId"),
    @NamedQuery(name = "HistorialEquipo.findByFecha", query = "SELECT h FROM HistorialEquipo h WHERE h.fecha = :fecha"),
    @NamedQuery(name = "HistorialEquipo.findByDetalle", query = "SELECT h FROM HistorialEquipo h WHERE h.detalle = :detalle"),
    @NamedQuery(name = "HistorialEquipo.findByValor", query = "SELECT h FROM HistorialEquipo h WHERE h.valor = :valor")})
public class HistorialEquipo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "HISTORIAL_EQUIPO_ID")
    private Long historialEquipoId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA")
    private long fecha;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "DETALLE")
    private String detalle;
    @Basic(optional = false)
    @NotNull
    @Column(name = "VALOR")
    private long valor;
    @JoinColumn(name = "USUARIO_ID", referencedColumnName = "USUARIO_ID")
    @ManyToOne(optional = false)
    private Usuario usuarioId;
    @JoinColumn(name = "ESTADO_ACTUALIZACION_ID", referencedColumnName = "ACTUALIZACION_ID")
    @ManyToOne(optional = false)
    private EstadoActualizacion estadoActualizacionId;
    @JoinColumn(name = "ENTRADA_ID", referencedColumnName = "ENTRADA_ID")
    @ManyToOne(optional = false)
    private Entrada entradaId;

    public HistorialEquipo() {
    }

    public HistorialEquipo(Long historialEquipoId) {
        this.historialEquipoId = historialEquipoId;
    }

    public HistorialEquipo(Long historialEquipoId, long fecha, String detalle, long valor) {
        this.historialEquipoId = historialEquipoId;
        this.fecha = fecha;
        this.detalle = detalle;
        this.valor = valor;
    }

    public Long getHistorialEquipoId() {
        return historialEquipoId;
    }

    public void setHistorialEquipoId(Long historialEquipoId) {
        this.historialEquipoId = historialEquipoId;
    }

    public long getFecha() {
        return fecha;
    }

    public void setFecha(long fecha) {
        this.fecha = fecha;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public long getValor() {
        return valor;
    }

    public void setValor(long valor) {
        this.valor = valor;
    }

    public Usuario getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Usuario usuarioId) {
        this.usuarioId = usuarioId;
    }

    public EstadoActualizacion getEstadoActualizacionId() {
        return estadoActualizacionId;
    }

    public void setEstadoActualizacionId(EstadoActualizacion estadoActualizacionId) {
        this.estadoActualizacionId = estadoActualizacionId;
    }

    public Entrada getEntradaId() {
        return entradaId;
    }

    public void setEntradaId(Entrada entradaId) {
        this.entradaId = entradaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (historialEquipoId != null ? historialEquipoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HistorialEquipo)) {
            return false;
        }
        HistorialEquipo other = (HistorialEquipo) object;
        if ((this.historialEquipoId == null && other.historialEquipoId != null) || (this.historialEquipoId != null && !this.historialEquipoId.equals(other.historialEquipoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entidades.HistorialEquipo[ historialEquipoId=" + historialEquipoId + " ]";
    }
    
}
