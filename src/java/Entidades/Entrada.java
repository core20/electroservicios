/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jorge
 */
@Entity
@Table(name = "ENTRADA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Entrada.findAll", query = "SELECT e FROM Entrada e"),
    @NamedQuery(name = "Entrada.findByEntradaId", query = "SELECT e FROM Entrada e WHERE e.entradaId = :entradaId"),
    @NamedQuery(name = "Entrada.findBySerie", query = "SELECT e FROM Entrada e WHERE e.serie = :serie"),
    @NamedQuery(name = "Entrada.findByModelo", query = "SELECT e FROM Entrada e WHERE e.modelo = :modelo"),
    @NamedQuery(name = "Entrada.findByFechaEntrada", query = "SELECT e FROM Entrada e WHERE e.fechaEntrada = :fechaEntrada"),
    @NamedQuery(name = "Entrada.findByFechaPrevistaEntrega", query = "SELECT e FROM Entrada e WHERE e.fechaPrevistaEntrega = :fechaPrevistaEntrega"),
    @NamedQuery(name = "Entrada.findByComentario", query = "SELECT e FROM Entrada e WHERE e.comentario = :comentario"),
    @NamedQuery(name = "Entrada.findByAccesorios", query = "SELECT e FROM Entrada e WHERE e.accesorios = :accesorios")})
public class Entrada implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ENTRADA_ID")
    private Long entradaId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "SERIE")
    private String serie;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "MODELO")
    private String modelo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_ENTRADA")
    private long fechaEntrada;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_PREVISTA_ENTREGA")
    private long fechaPrevistaEntrega;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "COMENTARIO")
    private String comentario;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ACCESORIOS")
    private String accesorios;
    @JoinColumn(name = "TIPO_EQUIPO_ID", referencedColumnName = "TIPO_ID")
    @ManyToOne(optional = false)
    private TipoEquipo tipoEquipoId;
    @JoinColumn(name = "MARCA_ID", referencedColumnName = "MARCA_ID")
    @ManyToOne(optional = false)
    private Marca marcaId;
    @JoinColumn(name = "RUT_CLIENTE", referencedColumnName = "RUT")
    @ManyToOne(optional = false)
    private Cliente rutCliente;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "entradaId")
    private Collection<HistorialEquipo> historialEquipoCollection;

    public Entrada() {
    }

    public Entrada(Long entradaId) {
        this.entradaId = entradaId;
    }

    public Entrada(Long entradaId, String serie, String modelo, long fechaEntrada, long fechaPrevistaEntrega, String comentario, String accesorios) {
        this.entradaId = entradaId;
        this.serie = serie;
        this.modelo = modelo;
        this.fechaEntrada = fechaEntrada;
        this.fechaPrevistaEntrega = fechaPrevistaEntrega;
        this.comentario = comentario;
        this.accesorios = accesorios;
    }

    public Long getEntradaId() {
        return entradaId;
    }

    public void setEntradaId(Long entradaId) {
        this.entradaId = entradaId;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public long getFechaEntrada() {
        return fechaEntrada;
    }

    public void setFechaEntrada(long fechaEntrada) {
        this.fechaEntrada = fechaEntrada;
    }

    public long getFechaPrevistaEntrega() {
        return fechaPrevistaEntrega;
    }

    public void setFechaPrevistaEntrega(long fechaPrevistaEntrega) {
        this.fechaPrevistaEntrega = fechaPrevistaEntrega;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getAccesorios() {
        return accesorios;
    }

    public void setAccesorios(String accesorios) {
        this.accesorios = accesorios;
    }

    public TipoEquipo getTipoEquipoId() {
        return tipoEquipoId;
    }

    public void setTipoEquipoId(TipoEquipo tipoEquipoId) {
        this.tipoEquipoId = tipoEquipoId;
    }

    public Marca getMarcaId() {
        return marcaId;
    }

    public void setMarcaId(Marca marcaId) {
        this.marcaId = marcaId;
    }

    public Cliente getRutCliente() {
        return rutCliente;
    }

    public void setRutCliente(Cliente rutCliente) {
        this.rutCliente = rutCliente;
    }

    @XmlTransient
    public Collection<HistorialEquipo> getHistorialEquipoCollection() {
        return historialEquipoCollection;
    }

    public void setHistorialEquipoCollection(Collection<HistorialEquipo> historialEquipoCollection) {
        this.historialEquipoCollection = historialEquipoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (entradaId != null ? entradaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Entrada)) {
            return false;
        }
        Entrada other = (Entrada) object;
        if ((this.entradaId == null && other.entradaId != null) || (this.entradaId != null && !this.entradaId.equals(other.entradaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entidades.Entrada[ entradaId=" + entradaId + " ]";
    }
    
}
