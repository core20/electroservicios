/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jorge
 */
@Entity
@Table(name = "ESTADO_ACTUALIZACION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EstadoActualizacion.findAll", query = "SELECT e FROM EstadoActualizacion e"),
    @NamedQuery(name = "EstadoActualizacion.findByActualizacionId", query = "SELECT e FROM EstadoActualizacion e WHERE e.actualizacionId = :actualizacionId"),
    @NamedQuery(name = "EstadoActualizacion.findByNombre", query = "SELECT e FROM EstadoActualizacion e WHERE e.nombre = :nombre"),
    @NamedQuery(name = "EstadoActualizacion.findByActualizacionTipo", query = "SELECT e FROM EstadoActualizacion e WHERE e.actualizacionTipo = :actualizacionTipo")})
public class EstadoActualizacion implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ACTUALIZACION_ID")
    private Long actualizacionId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NOMBRE")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ACTUALIZACION_TIPO")
    private long actualizacionTipo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "estadoActualizacionId")
    private Collection<HistorialEquipo> historialEquipoCollection;

    public EstadoActualizacion() {
    }

    public EstadoActualizacion(Long actualizacionId) {
        this.actualizacionId = actualizacionId;
    }

    public EstadoActualizacion(Long actualizacionId, String nombre, long actualizacionTipo) {
        this.actualizacionId = actualizacionId;
        this.nombre = nombre;
        this.actualizacionTipo = actualizacionTipo;
    }

    public Long getActualizacionId() {
        return actualizacionId;
    }

    public void setActualizacionId(Long actualizacionId) {
        this.actualizacionId = actualizacionId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public long getActualizacionTipo() {
        return actualizacionTipo;
    }

    public void setActualizacionTipo(long actualizacionTipo) {
        this.actualizacionTipo = actualizacionTipo;
    }

    @XmlTransient
    public Collection<HistorialEquipo> getHistorialEquipoCollection() {
        return historialEquipoCollection;
    }

    public void setHistorialEquipoCollection(Collection<HistorialEquipo> historialEquipoCollection) {
        this.historialEquipoCollection = historialEquipoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (actualizacionId != null ? actualizacionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EstadoActualizacion)) {
            return false;
        }
        EstadoActualizacion other = (EstadoActualizacion) object;
        if ((this.actualizacionId == null && other.actualizacionId != null) || (this.actualizacionId != null && !this.actualizacionId.equals(other.actualizacionId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entidades.EstadoActualizacion[ actualizacionId=" + actualizacionId + " ]";
    }
    
}
