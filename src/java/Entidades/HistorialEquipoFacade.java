/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author jorge
 */
@Stateless
public class HistorialEquipoFacade extends AbstractFacade<HistorialEquipo> {
    @PersistenceContext(unitName = "electroserviciosPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HistorialEquipoFacade() {
        super(HistorialEquipo.class);
    }
    
}
