/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jorge
 */
@Entity
@Table(name = "COMUNA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Comuna.findAll", query = "SELECT c FROM Comuna c"),
    @NamedQuery(name = "Comuna.findByCodigoComuna", query = "SELECT c FROM Comuna c WHERE c.codigoComuna = :codigoComuna"),
    @NamedQuery(name = "Comuna.findByNombre", query = "SELECT c FROM Comuna c WHERE c.nombre = :nombre"),
    /*Búsqueda agregada*/
    @NamedQuery(name = "Comuna.findByCodigoProvincia", query = "SELECT c FROM Comuna c WHERE c.codigoProvincia = :codigoProvincia")})
public class Comuna implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CODIGO_COMUNA")
    private Integer codigoComuna;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NOMBRE")
    private String nombre;
    @JoinColumn(name = "CODIGO_PROVINCIA", referencedColumnName = "CODIGO_PROVINCIA")
    @ManyToOne(optional = false)
    private Provincia codigoProvincia;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codigoComuna")
    private Collection<Cliente> clienteCollection;

    public Comuna() {
    }

    public Comuna(Integer codigoComuna) {
        this.codigoComuna = codigoComuna;
    }

    public Comuna(Integer codigoComuna, String nombre) {
        this.codigoComuna = codigoComuna;
        this.nombre = nombre;
    }

    public Integer getCodigoComuna() {
        return codigoComuna;
    }

    public void setCodigoComuna(Integer codigoComuna) {
        this.codigoComuna = codigoComuna;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Provincia getCodigoProvincia() {
        return codigoProvincia;
    }

    public void setCodigoProvincia(Provincia codigoProvincia) {
        this.codigoProvincia = codigoProvincia;
    }

    @XmlTransient
    public Collection<Cliente> getClienteCollection() {
        return clienteCollection;
    }

    public void setClienteCollection(Collection<Cliente> clienteCollection) {
        this.clienteCollection = clienteCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoComuna != null ? codigoComuna.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Comuna)) {
            return false;
        }
        Comuna other = (Comuna) object;
        if ((this.codigoComuna == null && other.codigoComuna != null) || (this.codigoComuna != null && !this.codigoComuna.equals(other.codigoComuna))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entidades.Comuna[ codigoComuna=" + codigoComuna + " ]";
    }
    
}
