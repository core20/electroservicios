/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jorge
 */
@Entity
@Table(name = "REGION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Region.findAll", query = "SELECT r FROM Region r"),
    @NamedQuery(name = "Region.findByCodigoRegion", query = "SELECT r FROM Region r WHERE r.codigoRegion = :codigoRegion"),
    @NamedQuery(name = "Region.findByNombre", query = "SELECT r FROM Region r WHERE r.nombre = :nombre")})
public class Region implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CODIGO_REGION")
    private Short codigoRegion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NOMBRE")
    private String nombre;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codigoRegion")
    private Collection<Provincia> provinciaCollection;

    public Region() {
    }

    public Region(Short codigoRegion) {
        this.codigoRegion = codigoRegion;
    }

    public Region(Short codigoRegion, String nombre) {
        this.codigoRegion = codigoRegion;
        this.nombre = nombre;
    }

    public Short getCodigoRegion() {
        return codigoRegion;
    }

    public void setCodigoRegion(Short codigoRegion) {
        this.codigoRegion = codigoRegion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @XmlTransient
    public Collection<Provincia> getProvinciaCollection() {
        return provinciaCollection;
    }

    public void setProvinciaCollection(Collection<Provincia> provinciaCollection) {
        this.provinciaCollection = provinciaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoRegion != null ? codigoRegion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Region)) {
            return false;
        }
        Region other = (Region) object;
        if ((this.codigoRegion == null && other.codigoRegion != null) || (this.codigoRegion != null && !this.codigoRegion.equals(other.codigoRegion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entidades.Region[ codigoRegion=" + codigoRegion + " ]";
    }
    
}
