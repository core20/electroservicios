/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Controlador.exceptions.IllegalOrphanException;
import Controlador.exceptions.NonexistentEntityException;
import Controlador.exceptions.PreexistingEntityException;
import Controlador.exceptions.RollbackFailureException;
import Entidades.EstadoActualizacion;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Entidades.HistorialEquipo;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

/**
 *
 * @author jorge
 */
public class EstadoActualizacionJpaController implements Serializable {

    public EstadoActualizacionJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(EstadoActualizacion estadoActualizacion) throws PreexistingEntityException, RollbackFailureException, Exception {
        if (estadoActualizacion.getHistorialEquipoCollection() == null) {
            estadoActualizacion.setHistorialEquipoCollection(new ArrayList<HistorialEquipo>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Collection<HistorialEquipo> attachedHistorialEquipoCollection = new ArrayList<HistorialEquipo>();
            for (HistorialEquipo historialEquipoCollectionHistorialEquipoToAttach : estadoActualizacion.getHistorialEquipoCollection()) {
                historialEquipoCollectionHistorialEquipoToAttach = em.getReference(historialEquipoCollectionHistorialEquipoToAttach.getClass(), historialEquipoCollectionHistorialEquipoToAttach.getHistorialEquipoId());
                attachedHistorialEquipoCollection.add(historialEquipoCollectionHistorialEquipoToAttach);
            }
            estadoActualizacion.setHistorialEquipoCollection(attachedHistorialEquipoCollection);
            em.persist(estadoActualizacion);
            for (HistorialEquipo historialEquipoCollectionHistorialEquipo : estadoActualizacion.getHistorialEquipoCollection()) {
                EstadoActualizacion oldEstadoActualizacionIdOfHistorialEquipoCollectionHistorialEquipo = historialEquipoCollectionHistorialEquipo.getEstadoActualizacionId();
                historialEquipoCollectionHistorialEquipo.setEstadoActualizacionId(estadoActualizacion);
                historialEquipoCollectionHistorialEquipo = em.merge(historialEquipoCollectionHistorialEquipo);
                if (oldEstadoActualizacionIdOfHistorialEquipoCollectionHistorialEquipo != null) {
                    oldEstadoActualizacionIdOfHistorialEquipoCollectionHistorialEquipo.getHistorialEquipoCollection().remove(historialEquipoCollectionHistorialEquipo);
                    oldEstadoActualizacionIdOfHistorialEquipoCollectionHistorialEquipo = em.merge(oldEstadoActualizacionIdOfHistorialEquipoCollectionHistorialEquipo);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findEstadoActualizacion(estadoActualizacion.getActualizacionId()) != null) {
                throw new PreexistingEntityException("EstadoActualizacion " + estadoActualizacion + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(EstadoActualizacion estadoActualizacion) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            EstadoActualizacion persistentEstadoActualizacion = em.find(EstadoActualizacion.class, estadoActualizacion.getActualizacionId());
            Collection<HistorialEquipo> historialEquipoCollectionOld = persistentEstadoActualizacion.getHistorialEquipoCollection();
            Collection<HistorialEquipo> historialEquipoCollectionNew = estadoActualizacion.getHistorialEquipoCollection();
            List<String> illegalOrphanMessages = null;
            for (HistorialEquipo historialEquipoCollectionOldHistorialEquipo : historialEquipoCollectionOld) {
                if (!historialEquipoCollectionNew.contains(historialEquipoCollectionOldHistorialEquipo)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain HistorialEquipo " + historialEquipoCollectionOldHistorialEquipo + " since its estadoActualizacionId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<HistorialEquipo> attachedHistorialEquipoCollectionNew = new ArrayList<HistorialEquipo>();
            for (HistorialEquipo historialEquipoCollectionNewHistorialEquipoToAttach : historialEquipoCollectionNew) {
                historialEquipoCollectionNewHistorialEquipoToAttach = em.getReference(historialEquipoCollectionNewHistorialEquipoToAttach.getClass(), historialEquipoCollectionNewHistorialEquipoToAttach.getHistorialEquipoId());
                attachedHistorialEquipoCollectionNew.add(historialEquipoCollectionNewHistorialEquipoToAttach);
            }
            historialEquipoCollectionNew = attachedHistorialEquipoCollectionNew;
            estadoActualizacion.setHistorialEquipoCollection(historialEquipoCollectionNew);
            estadoActualizacion = em.merge(estadoActualizacion);
            for (HistorialEquipo historialEquipoCollectionNewHistorialEquipo : historialEquipoCollectionNew) {
                if (!historialEquipoCollectionOld.contains(historialEquipoCollectionNewHistorialEquipo)) {
                    EstadoActualizacion oldEstadoActualizacionIdOfHistorialEquipoCollectionNewHistorialEquipo = historialEquipoCollectionNewHistorialEquipo.getEstadoActualizacionId();
                    historialEquipoCollectionNewHistorialEquipo.setEstadoActualizacionId(estadoActualizacion);
                    historialEquipoCollectionNewHistorialEquipo = em.merge(historialEquipoCollectionNewHistorialEquipo);
                    if (oldEstadoActualizacionIdOfHistorialEquipoCollectionNewHistorialEquipo != null && !oldEstadoActualizacionIdOfHistorialEquipoCollectionNewHistorialEquipo.equals(estadoActualizacion)) {
                        oldEstadoActualizacionIdOfHistorialEquipoCollectionNewHistorialEquipo.getHistorialEquipoCollection().remove(historialEquipoCollectionNewHistorialEquipo);
                        oldEstadoActualizacionIdOfHistorialEquipoCollectionNewHistorialEquipo = em.merge(oldEstadoActualizacionIdOfHistorialEquipoCollectionNewHistorialEquipo);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = estadoActualizacion.getActualizacionId();
                if (findEstadoActualizacion(id) == null) {
                    throw new NonexistentEntityException("The estadoActualizacion with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            EstadoActualizacion estadoActualizacion;
            try {
                estadoActualizacion = em.getReference(EstadoActualizacion.class, id);
                estadoActualizacion.getActualizacionId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The estadoActualizacion with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<HistorialEquipo> historialEquipoCollectionOrphanCheck = estadoActualizacion.getHistorialEquipoCollection();
            for (HistorialEquipo historialEquipoCollectionOrphanCheckHistorialEquipo : historialEquipoCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This EstadoActualizacion (" + estadoActualizacion + ") cannot be destroyed since the HistorialEquipo " + historialEquipoCollectionOrphanCheckHistorialEquipo + " in its historialEquipoCollection field has a non-nullable estadoActualizacionId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(estadoActualizacion);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<EstadoActualizacion> findEstadoActualizacionEntities() {
        return findEstadoActualizacionEntities(true, -1, -1);
    }

    public List<EstadoActualizacion> findEstadoActualizacionEntities(int maxResults, int firstResult) {
        return findEstadoActualizacionEntities(false, maxResults, firstResult);
    }

    private List<EstadoActualizacion> findEstadoActualizacionEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(EstadoActualizacion.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public EstadoActualizacion findEstadoActualizacion(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(EstadoActualizacion.class, id);
        } finally {
            em.close();
        }
    }

    public int getEstadoActualizacionCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<EstadoActualizacion> rt = cq.from(EstadoActualizacion.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
