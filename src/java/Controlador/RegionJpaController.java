/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Controlador.exceptions.IllegalOrphanException;
import Controlador.exceptions.NonexistentEntityException;
import Controlador.exceptions.PreexistingEntityException;
import Controlador.exceptions.RollbackFailureException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Entidades.Provincia;
import Entidades.Region;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

/**
 *
 * @author jorge
 */
public class RegionJpaController implements Serializable {

    public RegionJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Region region) throws PreexistingEntityException, RollbackFailureException, Exception {
        if (region.getProvinciaCollection() == null) {
            region.setProvinciaCollection(new ArrayList<Provincia>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Collection<Provincia> attachedProvinciaCollection = new ArrayList<Provincia>();
            for (Provincia provinciaCollectionProvinciaToAttach : region.getProvinciaCollection()) {
                provinciaCollectionProvinciaToAttach = em.getReference(provinciaCollectionProvinciaToAttach.getClass(), provinciaCollectionProvinciaToAttach.getCodigoProvincia());
                attachedProvinciaCollection.add(provinciaCollectionProvinciaToAttach);
            }
            region.setProvinciaCollection(attachedProvinciaCollection);
            em.persist(region);
            for (Provincia provinciaCollectionProvincia : region.getProvinciaCollection()) {
                Region oldCodigoRegionOfProvinciaCollectionProvincia = provinciaCollectionProvincia.getCodigoRegion();
                provinciaCollectionProvincia.setCodigoRegion(region);
                provinciaCollectionProvincia = em.merge(provinciaCollectionProvincia);
                if (oldCodigoRegionOfProvinciaCollectionProvincia != null) {
                    oldCodigoRegionOfProvinciaCollectionProvincia.getProvinciaCollection().remove(provinciaCollectionProvincia);
                    oldCodigoRegionOfProvinciaCollectionProvincia = em.merge(oldCodigoRegionOfProvinciaCollectionProvincia);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findRegion(region.getCodigoRegion()) != null) {
                throw new PreexistingEntityException("Region " + region + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Region region) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Region persistentRegion = em.find(Region.class, region.getCodigoRegion());
            Collection<Provincia> provinciaCollectionOld = persistentRegion.getProvinciaCollection();
            Collection<Provincia> provinciaCollectionNew = region.getProvinciaCollection();
            List<String> illegalOrphanMessages = null;
            for (Provincia provinciaCollectionOldProvincia : provinciaCollectionOld) {
                if (!provinciaCollectionNew.contains(provinciaCollectionOldProvincia)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Provincia " + provinciaCollectionOldProvincia + " since its codigoRegion field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Provincia> attachedProvinciaCollectionNew = new ArrayList<Provincia>();
            for (Provincia provinciaCollectionNewProvinciaToAttach : provinciaCollectionNew) {
                provinciaCollectionNewProvinciaToAttach = em.getReference(provinciaCollectionNewProvinciaToAttach.getClass(), provinciaCollectionNewProvinciaToAttach.getCodigoProvincia());
                attachedProvinciaCollectionNew.add(provinciaCollectionNewProvinciaToAttach);
            }
            provinciaCollectionNew = attachedProvinciaCollectionNew;
            region.setProvinciaCollection(provinciaCollectionNew);
            region = em.merge(region);
            for (Provincia provinciaCollectionNewProvincia : provinciaCollectionNew) {
                if (!provinciaCollectionOld.contains(provinciaCollectionNewProvincia)) {
                    Region oldCodigoRegionOfProvinciaCollectionNewProvincia = provinciaCollectionNewProvincia.getCodigoRegion();
                    provinciaCollectionNewProvincia.setCodigoRegion(region);
                    provinciaCollectionNewProvincia = em.merge(provinciaCollectionNewProvincia);
                    if (oldCodigoRegionOfProvinciaCollectionNewProvincia != null && !oldCodigoRegionOfProvinciaCollectionNewProvincia.equals(region)) {
                        oldCodigoRegionOfProvinciaCollectionNewProvincia.getProvinciaCollection().remove(provinciaCollectionNewProvincia);
                        oldCodigoRegionOfProvinciaCollectionNewProvincia = em.merge(oldCodigoRegionOfProvinciaCollectionNewProvincia);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Short id = region.getCodigoRegion();
                if (findRegion(id) == null) {
                    throw new NonexistentEntityException("The region with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Short id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Region region;
            try {
                region = em.getReference(Region.class, id);
                region.getCodigoRegion();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The region with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Provincia> provinciaCollectionOrphanCheck = region.getProvinciaCollection();
            for (Provincia provinciaCollectionOrphanCheckProvincia : provinciaCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Region (" + region + ") cannot be destroyed since the Provincia " + provinciaCollectionOrphanCheckProvincia + " in its provinciaCollection field has a non-nullable codigoRegion field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(region);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Region> findRegionEntities() {
        return findRegionEntities(true, -1, -1);
    }

    public List<Region> findRegionEntities(int maxResults, int firstResult) {
        return findRegionEntities(false, maxResults, firstResult);
    }

    private List<Region> findRegionEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Region.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Region findRegion(Short id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Region.class, id);
        } finally {
            em.close();
        }
    }

    public int getRegionCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Region> rt = cq.from(Region.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
