/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Controlador.exceptions.IllegalOrphanException;
import Controlador.exceptions.NonexistentEntityException;
import Controlador.exceptions.PreexistingEntityException;
import Controlador.exceptions.RollbackFailureException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Entidades.TipoEquipo;
import Entidades.Marca;
import Entidades.Cliente;
import Entidades.Entrada;
import Entidades.HistorialEquipo;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

/**
 *
 * @author jorge
 */
public class EntradaJpaController implements Serializable {

    public EntradaJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Entrada entrada) throws PreexistingEntityException, RollbackFailureException, Exception {
        if (entrada.getHistorialEquipoCollection() == null) {
            entrada.setHistorialEquipoCollection(new ArrayList<HistorialEquipo>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            TipoEquipo tipoEquipoId = entrada.getTipoEquipoId();
            if (tipoEquipoId != null) {
                tipoEquipoId = em.getReference(tipoEquipoId.getClass(), tipoEquipoId.getTipoId());
                entrada.setTipoEquipoId(tipoEquipoId);
            }
            Marca marcaId = entrada.getMarcaId();
            if (marcaId != null) {
                marcaId = em.getReference(marcaId.getClass(), marcaId.getMarcaId());
                entrada.setMarcaId(marcaId);
            }
            Cliente rutCliente = entrada.getRutCliente();
            if (rutCliente != null) {
                rutCliente = em.getReference(rutCliente.getClass(), rutCliente.getRut());
                entrada.setRutCliente(rutCliente);
            }
            Collection<HistorialEquipo> attachedHistorialEquipoCollection = new ArrayList<HistorialEquipo>();
            for (HistorialEquipo historialEquipoCollectionHistorialEquipoToAttach : entrada.getHistorialEquipoCollection()) {
                historialEquipoCollectionHistorialEquipoToAttach = em.getReference(historialEquipoCollectionHistorialEquipoToAttach.getClass(), historialEquipoCollectionHistorialEquipoToAttach.getHistorialEquipoId());
                attachedHistorialEquipoCollection.add(historialEquipoCollectionHistorialEquipoToAttach);
            }
            entrada.setHistorialEquipoCollection(attachedHistorialEquipoCollection);
            em.persist(entrada);
            if (tipoEquipoId != null) {
                tipoEquipoId.getEntradaCollection().add(entrada);
                tipoEquipoId = em.merge(tipoEquipoId);
            }
            if (marcaId != null) {
                marcaId.getEntradaCollection().add(entrada);
                marcaId = em.merge(marcaId);
            }
            if (rutCliente != null) {
                rutCliente.getEntradaCollection().add(entrada);
                rutCliente = em.merge(rutCliente);
            }
            for (HistorialEquipo historialEquipoCollectionHistorialEquipo : entrada.getHistorialEquipoCollection()) {
                Entrada oldEntradaIdOfHistorialEquipoCollectionHistorialEquipo = historialEquipoCollectionHistorialEquipo.getEntradaId();
                historialEquipoCollectionHistorialEquipo.setEntradaId(entrada);
                historialEquipoCollectionHistorialEquipo = em.merge(historialEquipoCollectionHistorialEquipo);
                if (oldEntradaIdOfHistorialEquipoCollectionHistorialEquipo != null) {
                    oldEntradaIdOfHistorialEquipoCollectionHistorialEquipo.getHistorialEquipoCollection().remove(historialEquipoCollectionHistorialEquipo);
                    oldEntradaIdOfHistorialEquipoCollectionHistorialEquipo = em.merge(oldEntradaIdOfHistorialEquipoCollectionHistorialEquipo);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findEntrada(entrada.getEntradaId()) != null) {
                throw new PreexistingEntityException("Entrada " + entrada + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Entrada entrada) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Entrada persistentEntrada = em.find(Entrada.class, entrada.getEntradaId());
            TipoEquipo tipoEquipoIdOld = persistentEntrada.getTipoEquipoId();
            TipoEquipo tipoEquipoIdNew = entrada.getTipoEquipoId();
            Marca marcaIdOld = persistentEntrada.getMarcaId();
            Marca marcaIdNew = entrada.getMarcaId();
            Cliente rutClienteOld = persistentEntrada.getRutCliente();
            Cliente rutClienteNew = entrada.getRutCliente();
            Collection<HistorialEquipo> historialEquipoCollectionOld = persistentEntrada.getHistorialEquipoCollection();
            Collection<HistorialEquipo> historialEquipoCollectionNew = entrada.getHistorialEquipoCollection();
            List<String> illegalOrphanMessages = null;
            for (HistorialEquipo historialEquipoCollectionOldHistorialEquipo : historialEquipoCollectionOld) {
                if (!historialEquipoCollectionNew.contains(historialEquipoCollectionOldHistorialEquipo)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain HistorialEquipo " + historialEquipoCollectionOldHistorialEquipo + " since its entradaId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (tipoEquipoIdNew != null) {
                tipoEquipoIdNew = em.getReference(tipoEquipoIdNew.getClass(), tipoEquipoIdNew.getTipoId());
                entrada.setTipoEquipoId(tipoEquipoIdNew);
            }
            if (marcaIdNew != null) {
                marcaIdNew = em.getReference(marcaIdNew.getClass(), marcaIdNew.getMarcaId());
                entrada.setMarcaId(marcaIdNew);
            }
            if (rutClienteNew != null) {
                rutClienteNew = em.getReference(rutClienteNew.getClass(), rutClienteNew.getRut());
                entrada.setRutCliente(rutClienteNew);
            }
            Collection<HistorialEquipo> attachedHistorialEquipoCollectionNew = new ArrayList<HistorialEquipo>();
            for (HistorialEquipo historialEquipoCollectionNewHistorialEquipoToAttach : historialEquipoCollectionNew) {
                historialEquipoCollectionNewHistorialEquipoToAttach = em.getReference(historialEquipoCollectionNewHistorialEquipoToAttach.getClass(), historialEquipoCollectionNewHistorialEquipoToAttach.getHistorialEquipoId());
                attachedHistorialEquipoCollectionNew.add(historialEquipoCollectionNewHistorialEquipoToAttach);
            }
            historialEquipoCollectionNew = attachedHistorialEquipoCollectionNew;
            entrada.setHistorialEquipoCollection(historialEquipoCollectionNew);
            entrada = em.merge(entrada);
            if (tipoEquipoIdOld != null && !tipoEquipoIdOld.equals(tipoEquipoIdNew)) {
                tipoEquipoIdOld.getEntradaCollection().remove(entrada);
                tipoEquipoIdOld = em.merge(tipoEquipoIdOld);
            }
            if (tipoEquipoIdNew != null && !tipoEquipoIdNew.equals(tipoEquipoIdOld)) {
                tipoEquipoIdNew.getEntradaCollection().add(entrada);
                tipoEquipoIdNew = em.merge(tipoEquipoIdNew);
            }
            if (marcaIdOld != null && !marcaIdOld.equals(marcaIdNew)) {
                marcaIdOld.getEntradaCollection().remove(entrada);
                marcaIdOld = em.merge(marcaIdOld);
            }
            if (marcaIdNew != null && !marcaIdNew.equals(marcaIdOld)) {
                marcaIdNew.getEntradaCollection().add(entrada);
                marcaIdNew = em.merge(marcaIdNew);
            }
            if (rutClienteOld != null && !rutClienteOld.equals(rutClienteNew)) {
                rutClienteOld.getEntradaCollection().remove(entrada);
                rutClienteOld = em.merge(rutClienteOld);
            }
            if (rutClienteNew != null && !rutClienteNew.equals(rutClienteOld)) {
                rutClienteNew.getEntradaCollection().add(entrada);
                rutClienteNew = em.merge(rutClienteNew);
            }
            for (HistorialEquipo historialEquipoCollectionNewHistorialEquipo : historialEquipoCollectionNew) {
                if (!historialEquipoCollectionOld.contains(historialEquipoCollectionNewHistorialEquipo)) {
                    Entrada oldEntradaIdOfHistorialEquipoCollectionNewHistorialEquipo = historialEquipoCollectionNewHistorialEquipo.getEntradaId();
                    historialEquipoCollectionNewHistorialEquipo.setEntradaId(entrada);
                    historialEquipoCollectionNewHistorialEquipo = em.merge(historialEquipoCollectionNewHistorialEquipo);
                    if (oldEntradaIdOfHistorialEquipoCollectionNewHistorialEquipo != null && !oldEntradaIdOfHistorialEquipoCollectionNewHistorialEquipo.equals(entrada)) {
                        oldEntradaIdOfHistorialEquipoCollectionNewHistorialEquipo.getHistorialEquipoCollection().remove(historialEquipoCollectionNewHistorialEquipo);
                        oldEntradaIdOfHistorialEquipoCollectionNewHistorialEquipo = em.merge(oldEntradaIdOfHistorialEquipoCollectionNewHistorialEquipo);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = entrada.getEntradaId();
                if (findEntrada(id) == null) {
                    throw new NonexistentEntityException("The entrada with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Entrada entrada;
            try {
                entrada = em.getReference(Entrada.class, id);
                entrada.getEntradaId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The entrada with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<HistorialEquipo> historialEquipoCollectionOrphanCheck = entrada.getHistorialEquipoCollection();
            for (HistorialEquipo historialEquipoCollectionOrphanCheckHistorialEquipo : historialEquipoCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Entrada (" + entrada + ") cannot be destroyed since the HistorialEquipo " + historialEquipoCollectionOrphanCheckHistorialEquipo + " in its historialEquipoCollection field has a non-nullable entradaId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            TipoEquipo tipoEquipoId = entrada.getTipoEquipoId();
            if (tipoEquipoId != null) {
                tipoEquipoId.getEntradaCollection().remove(entrada);
                tipoEquipoId = em.merge(tipoEquipoId);
            }
            Marca marcaId = entrada.getMarcaId();
            if (marcaId != null) {
                marcaId.getEntradaCollection().remove(entrada);
                marcaId = em.merge(marcaId);
            }
            Cliente rutCliente = entrada.getRutCliente();
            if (rutCliente != null) {
                rutCliente.getEntradaCollection().remove(entrada);
                rutCliente = em.merge(rutCliente);
            }
            em.remove(entrada);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Entrada> findEntradaEntities() {
        return findEntradaEntities(true, -1, -1);
    }

    public List<Entrada> findEntradaEntities(int maxResults, int firstResult) {
        return findEntradaEntities(false, maxResults, firstResult);
    }

    private List<Entrada> findEntradaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Entrada.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Entrada findEntrada(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Entrada.class, id);
        } finally {
            em.close();
        }
    }

    public int getEntradaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Entrada> rt = cq.from(Entrada.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
