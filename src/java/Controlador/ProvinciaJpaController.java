/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Controlador.exceptions.IllegalOrphanException;
import Controlador.exceptions.NonexistentEntityException;
import Controlador.exceptions.PreexistingEntityException;
import Controlador.exceptions.RollbackFailureException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Entidades.Region;
import Entidades.Comuna;
import Entidades.Provincia;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.transaction.UserTransaction;

/**
 *
 * @author jorge
 */
public class ProvinciaJpaController implements Serializable {

    public ProvinciaJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Provincia provincia) throws PreexistingEntityException, RollbackFailureException, Exception {
        if (provincia.getComunaCollection() == null) {
            provincia.setComunaCollection(new ArrayList<Comuna>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Region codigoRegion = provincia.getCodigoRegion();
            if (codigoRegion != null) {
                codigoRegion = em.getReference(codigoRegion.getClass(), codigoRegion.getCodigoRegion());
                provincia.setCodigoRegion(codigoRegion);
            }
            Collection<Comuna> attachedComunaCollection = new ArrayList<Comuna>();
            for (Comuna comunaCollectionComunaToAttach : provincia.getComunaCollection()) {
                comunaCollectionComunaToAttach = em.getReference(comunaCollectionComunaToAttach.getClass(), comunaCollectionComunaToAttach.getCodigoComuna());
                attachedComunaCollection.add(comunaCollectionComunaToAttach);
            }
            provincia.setComunaCollection(attachedComunaCollection);
            em.persist(provincia);
            if (codigoRegion != null) {
                codigoRegion.getProvinciaCollection().add(provincia);
                codigoRegion = em.merge(codigoRegion);
            }
            for (Comuna comunaCollectionComuna : provincia.getComunaCollection()) {
                Provincia oldCodigoProvinciaOfComunaCollectionComuna = comunaCollectionComuna.getCodigoProvincia();
                comunaCollectionComuna.setCodigoProvincia(provincia);
                comunaCollectionComuna = em.merge(comunaCollectionComuna);
                if (oldCodigoProvinciaOfComunaCollectionComuna != null) {
                    oldCodigoProvinciaOfComunaCollectionComuna.getComunaCollection().remove(comunaCollectionComuna);
                    oldCodigoProvinciaOfComunaCollectionComuna = em.merge(oldCodigoProvinciaOfComunaCollectionComuna);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findProvincia(provincia.getCodigoProvincia()) != null) {
                throw new PreexistingEntityException("Provincia " + provincia + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Provincia provincia) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Provincia persistentProvincia = em.find(Provincia.class, provincia.getCodigoProvincia());
            Region codigoRegionOld = persistentProvincia.getCodigoRegion();
            Region codigoRegionNew = provincia.getCodigoRegion();
            Collection<Comuna> comunaCollectionOld = persistentProvincia.getComunaCollection();
            Collection<Comuna> comunaCollectionNew = provincia.getComunaCollection();
            List<String> illegalOrphanMessages = null;
            for (Comuna comunaCollectionOldComuna : comunaCollectionOld) {
                if (!comunaCollectionNew.contains(comunaCollectionOldComuna)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Comuna " + comunaCollectionOldComuna + " since its codigoProvincia field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (codigoRegionNew != null) {
                codigoRegionNew = em.getReference(codigoRegionNew.getClass(), codigoRegionNew.getCodigoRegion());
                provincia.setCodigoRegion(codigoRegionNew);
            }
            Collection<Comuna> attachedComunaCollectionNew = new ArrayList<Comuna>();
            for (Comuna comunaCollectionNewComunaToAttach : comunaCollectionNew) {
                comunaCollectionNewComunaToAttach = em.getReference(comunaCollectionNewComunaToAttach.getClass(), comunaCollectionNewComunaToAttach.getCodigoComuna());
                attachedComunaCollectionNew.add(comunaCollectionNewComunaToAttach);
            }
            comunaCollectionNew = attachedComunaCollectionNew;
            provincia.setComunaCollection(comunaCollectionNew);
            provincia = em.merge(provincia);
            if (codigoRegionOld != null && !codigoRegionOld.equals(codigoRegionNew)) {
                codigoRegionOld.getProvinciaCollection().remove(provincia);
                codigoRegionOld = em.merge(codigoRegionOld);
            }
            if (codigoRegionNew != null && !codigoRegionNew.equals(codigoRegionOld)) {
                codigoRegionNew.getProvinciaCollection().add(provincia);
                codigoRegionNew = em.merge(codigoRegionNew);
            }
            for (Comuna comunaCollectionNewComuna : comunaCollectionNew) {
                if (!comunaCollectionOld.contains(comunaCollectionNewComuna)) {
                    Provincia oldCodigoProvinciaOfComunaCollectionNewComuna = comunaCollectionNewComuna.getCodigoProvincia();
                    comunaCollectionNewComuna.setCodigoProvincia(provincia);
                    comunaCollectionNewComuna = em.merge(comunaCollectionNewComuna);
                    if (oldCodigoProvinciaOfComunaCollectionNewComuna != null && !oldCodigoProvinciaOfComunaCollectionNewComuna.equals(provincia)) {
                        oldCodigoProvinciaOfComunaCollectionNewComuna.getComunaCollection().remove(comunaCollectionNewComuna);
                        oldCodigoProvinciaOfComunaCollectionNewComuna = em.merge(oldCodigoProvinciaOfComunaCollectionNewComuna);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Short id = provincia.getCodigoProvincia();
                if (findProvincia(id) == null) {
                    throw new NonexistentEntityException("The provincia with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Short id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Provincia provincia;
            try {
                provincia = em.getReference(Provincia.class, id);
                provincia.getCodigoProvincia();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The provincia with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Comuna> comunaCollectionOrphanCheck = provincia.getComunaCollection();
            for (Comuna comunaCollectionOrphanCheckComuna : comunaCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Provincia (" + provincia + ") cannot be destroyed since the Comuna " + comunaCollectionOrphanCheckComuna + " in its comunaCollection field has a non-nullable codigoProvincia field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Region codigoRegion = provincia.getCodigoRegion();
            if (codigoRegion != null) {
                codigoRegion.getProvinciaCollection().remove(provincia);
                codigoRegion = em.merge(codigoRegion);
            }
            em.remove(provincia);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Provincia> findProvinciaEntities() {
        return findProvinciaEntities(true, -1, -1);
    }

    public List<Provincia> findProvinciaEntities(int maxResults, int firstResult) {
        return findProvinciaEntities(false, maxResults, firstResult);
    }

    private List<Provincia> findProvinciaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Provincia.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Provincia findProvincia(Short id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Provincia.class, id);
        } finally {
            em.close();
        }
    }

    public int getProvinciaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Provincia> rt = cq.from(Provincia.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    /*Funcion agregada*/
    public List<Provincia> findProvinciaEntitiesByCodigoRegion(Region codigoRegion) {
        EntityManager em = getEntityManager();
        try {
            Query consulta = em.createNamedQuery("Provincia.findByCodigoRegion").setParameter("codigoRegion", codigoRegion);
            try{
                return consulta.getResultList();
            } catch (NoResultException ex) {
                return null;//Exepción para retornar null si no hay resultados
            }
        } finally {
            em.close();
        }
    }
    
}
