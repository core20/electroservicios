/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Controlador.exceptions.IllegalOrphanException;
import Controlador.exceptions.NonexistentEntityException;
import Controlador.exceptions.PreexistingEntityException;
import Controlador.exceptions.RollbackFailureException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Entidades.Entrada;
import Entidades.TipoEquipo;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

/**
 *
 * @author jorge
 */
public class TipoEquipoJpaController implements Serializable {

    public TipoEquipoJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TipoEquipo tipoEquipo) throws PreexistingEntityException, RollbackFailureException, Exception {
        if (tipoEquipo.getEntradaCollection() == null) {
            tipoEquipo.setEntradaCollection(new ArrayList<Entrada>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Collection<Entrada> attachedEntradaCollection = new ArrayList<Entrada>();
            for (Entrada entradaCollectionEntradaToAttach : tipoEquipo.getEntradaCollection()) {
                entradaCollectionEntradaToAttach = em.getReference(entradaCollectionEntradaToAttach.getClass(), entradaCollectionEntradaToAttach.getEntradaId());
                attachedEntradaCollection.add(entradaCollectionEntradaToAttach);
            }
            tipoEquipo.setEntradaCollection(attachedEntradaCollection);
            em.persist(tipoEquipo);
            for (Entrada entradaCollectionEntrada : tipoEquipo.getEntradaCollection()) {
                TipoEquipo oldTipoEquipoIdOfEntradaCollectionEntrada = entradaCollectionEntrada.getTipoEquipoId();
                entradaCollectionEntrada.setTipoEquipoId(tipoEquipo);
                entradaCollectionEntrada = em.merge(entradaCollectionEntrada);
                if (oldTipoEquipoIdOfEntradaCollectionEntrada != null) {
                    oldTipoEquipoIdOfEntradaCollectionEntrada.getEntradaCollection().remove(entradaCollectionEntrada);
                    oldTipoEquipoIdOfEntradaCollectionEntrada = em.merge(oldTipoEquipoIdOfEntradaCollectionEntrada);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findTipoEquipo(tipoEquipo.getTipoId()) != null) {
                throw new PreexistingEntityException("TipoEquipo " + tipoEquipo + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TipoEquipo tipoEquipo) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            TipoEquipo persistentTipoEquipo = em.find(TipoEquipo.class, tipoEquipo.getTipoId());
            Collection<Entrada> entradaCollectionOld = persistentTipoEquipo.getEntradaCollection();
            Collection<Entrada> entradaCollectionNew = tipoEquipo.getEntradaCollection();
            List<String> illegalOrphanMessages = null;
            for (Entrada entradaCollectionOldEntrada : entradaCollectionOld) {
                if (!entradaCollectionNew.contains(entradaCollectionOldEntrada)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Entrada " + entradaCollectionOldEntrada + " since its tipoEquipoId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Entrada> attachedEntradaCollectionNew = new ArrayList<Entrada>();
            for (Entrada entradaCollectionNewEntradaToAttach : entradaCollectionNew) {
                entradaCollectionNewEntradaToAttach = em.getReference(entradaCollectionNewEntradaToAttach.getClass(), entradaCollectionNewEntradaToAttach.getEntradaId());
                attachedEntradaCollectionNew.add(entradaCollectionNewEntradaToAttach);
            }
            entradaCollectionNew = attachedEntradaCollectionNew;
            tipoEquipo.setEntradaCollection(entradaCollectionNew);
            tipoEquipo = em.merge(tipoEquipo);
            for (Entrada entradaCollectionNewEntrada : entradaCollectionNew) {
                if (!entradaCollectionOld.contains(entradaCollectionNewEntrada)) {
                    TipoEquipo oldTipoEquipoIdOfEntradaCollectionNewEntrada = entradaCollectionNewEntrada.getTipoEquipoId();
                    entradaCollectionNewEntrada.setTipoEquipoId(tipoEquipo);
                    entradaCollectionNewEntrada = em.merge(entradaCollectionNewEntrada);
                    if (oldTipoEquipoIdOfEntradaCollectionNewEntrada != null && !oldTipoEquipoIdOfEntradaCollectionNewEntrada.equals(tipoEquipo)) {
                        oldTipoEquipoIdOfEntradaCollectionNewEntrada.getEntradaCollection().remove(entradaCollectionNewEntrada);
                        oldTipoEquipoIdOfEntradaCollectionNewEntrada = em.merge(oldTipoEquipoIdOfEntradaCollectionNewEntrada);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = tipoEquipo.getTipoId();
                if (findTipoEquipo(id) == null) {
                    throw new NonexistentEntityException("The tipoEquipo with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            TipoEquipo tipoEquipo;
            try {
                tipoEquipo = em.getReference(TipoEquipo.class, id);
                tipoEquipo.getTipoId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tipoEquipo with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Entrada> entradaCollectionOrphanCheck = tipoEquipo.getEntradaCollection();
            for (Entrada entradaCollectionOrphanCheckEntrada : entradaCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TipoEquipo (" + tipoEquipo + ") cannot be destroyed since the Entrada " + entradaCollectionOrphanCheckEntrada + " in its entradaCollection field has a non-nullable tipoEquipoId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(tipoEquipo);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TipoEquipo> findTipoEquipoEntities() {
        return findTipoEquipoEntities(true, -1, -1);
    }

    public List<TipoEquipo> findTipoEquipoEntities(int maxResults, int firstResult) {
        return findTipoEquipoEntities(false, maxResults, firstResult);
    }

    private List<TipoEquipo> findTipoEquipoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TipoEquipo.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TipoEquipo findTipoEquipo(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TipoEquipo.class, id);
        } finally {
            em.close();
        }
    }

    public int getTipoEquipoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TipoEquipo> rt = cq.from(TipoEquipo.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
