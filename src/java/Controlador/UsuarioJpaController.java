/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Controlador.exceptions.IllegalOrphanException;
import Controlador.exceptions.NonexistentEntityException;
import Controlador.exceptions.PreexistingEntityException;
import Controlador.exceptions.RollbackFailureException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Entidades.Empleado;
import Entidades.HistorialEquipo;
import Entidades.Usuario;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.transaction.UserTransaction;

/**
 *
 * @author jorge
 */
public class UsuarioJpaController implements Serializable {

    public UsuarioJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Usuario usuario) throws PreexistingEntityException, RollbackFailureException, Exception {
        if (usuario.getHistorialEquipoCollection() == null) {
            usuario.setHistorialEquipoCollection(new ArrayList<HistorialEquipo>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Empleado rutEmpleado = usuario.getRutEmpleado();
            if (rutEmpleado != null) {
                rutEmpleado = em.getReference(rutEmpleado.getClass(), rutEmpleado.getRut());
                usuario.setRutEmpleado(rutEmpleado);
            }
            Collection<HistorialEquipo> attachedHistorialEquipoCollection = new ArrayList<HistorialEquipo>();
            for (HistorialEquipo historialEquipoCollectionHistorialEquipoToAttach : usuario.getHistorialEquipoCollection()) {
                historialEquipoCollectionHistorialEquipoToAttach = em.getReference(historialEquipoCollectionHistorialEquipoToAttach.getClass(), historialEquipoCollectionHistorialEquipoToAttach.getHistorialEquipoId());
                attachedHistorialEquipoCollection.add(historialEquipoCollectionHistorialEquipoToAttach);
            }
            usuario.setHistorialEquipoCollection(attachedHistorialEquipoCollection);
            em.persist(usuario);
            if (rutEmpleado != null) {
                rutEmpleado.getUsuarioCollection().add(usuario);
                rutEmpleado = em.merge(rutEmpleado);
            }
            for (HistorialEquipo historialEquipoCollectionHistorialEquipo : usuario.getHistorialEquipoCollection()) {
                Usuario oldUsuarioIdOfHistorialEquipoCollectionHistorialEquipo = historialEquipoCollectionHistorialEquipo.getUsuarioId();
                historialEquipoCollectionHistorialEquipo.setUsuarioId(usuario);
                historialEquipoCollectionHistorialEquipo = em.merge(historialEquipoCollectionHistorialEquipo);
                if (oldUsuarioIdOfHistorialEquipoCollectionHistorialEquipo != null) {
                    oldUsuarioIdOfHistorialEquipoCollectionHistorialEquipo.getHistorialEquipoCollection().remove(historialEquipoCollectionHistorialEquipo);
                    oldUsuarioIdOfHistorialEquipoCollectionHistorialEquipo = em.merge(oldUsuarioIdOfHistorialEquipoCollectionHistorialEquipo);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findUsuario(usuario.getUsuarioId()) != null) {
                throw new PreexistingEntityException("Usuario " + usuario + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Usuario usuario) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Usuario persistentUsuario = em.find(Usuario.class, usuario.getUsuarioId());
            Empleado rutEmpleadoOld = persistentUsuario.getRutEmpleado();
            Empleado rutEmpleadoNew = usuario.getRutEmpleado();
            Collection<HistorialEquipo> historialEquipoCollectionOld = persistentUsuario.getHistorialEquipoCollection();
            Collection<HistorialEquipo> historialEquipoCollectionNew = usuario.getHistorialEquipoCollection();
            List<String> illegalOrphanMessages = null;
            for (HistorialEquipo historialEquipoCollectionOldHistorialEquipo : historialEquipoCollectionOld) {
                if (!historialEquipoCollectionNew.contains(historialEquipoCollectionOldHistorialEquipo)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain HistorialEquipo " + historialEquipoCollectionOldHistorialEquipo + " since its usuarioId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (rutEmpleadoNew != null) {
                rutEmpleadoNew = em.getReference(rutEmpleadoNew.getClass(), rutEmpleadoNew.getRut());
                usuario.setRutEmpleado(rutEmpleadoNew);
            }
            Collection<HistorialEquipo> attachedHistorialEquipoCollectionNew = new ArrayList<HistorialEquipo>();
            for (HistorialEquipo historialEquipoCollectionNewHistorialEquipoToAttach : historialEquipoCollectionNew) {
                historialEquipoCollectionNewHistorialEquipoToAttach = em.getReference(historialEquipoCollectionNewHistorialEquipoToAttach.getClass(), historialEquipoCollectionNewHistorialEquipoToAttach.getHistorialEquipoId());
                attachedHistorialEquipoCollectionNew.add(historialEquipoCollectionNewHistorialEquipoToAttach);
            }
            historialEquipoCollectionNew = attachedHistorialEquipoCollectionNew;
            usuario.setHistorialEquipoCollection(historialEquipoCollectionNew);
            usuario = em.merge(usuario);
            if (rutEmpleadoOld != null && !rutEmpleadoOld.equals(rutEmpleadoNew)) {
                rutEmpleadoOld.getUsuarioCollection().remove(usuario);
                rutEmpleadoOld = em.merge(rutEmpleadoOld);
            }
            if (rutEmpleadoNew != null && !rutEmpleadoNew.equals(rutEmpleadoOld)) {
                rutEmpleadoNew.getUsuarioCollection().add(usuario);
                rutEmpleadoNew = em.merge(rutEmpleadoNew);
            }
            for (HistorialEquipo historialEquipoCollectionNewHistorialEquipo : historialEquipoCollectionNew) {
                if (!historialEquipoCollectionOld.contains(historialEquipoCollectionNewHistorialEquipo)) {
                    Usuario oldUsuarioIdOfHistorialEquipoCollectionNewHistorialEquipo = historialEquipoCollectionNewHistorialEquipo.getUsuarioId();
                    historialEquipoCollectionNewHistorialEquipo.setUsuarioId(usuario);
                    historialEquipoCollectionNewHistorialEquipo = em.merge(historialEquipoCollectionNewHistorialEquipo);
                    if (oldUsuarioIdOfHistorialEquipoCollectionNewHistorialEquipo != null && !oldUsuarioIdOfHistorialEquipoCollectionNewHistorialEquipo.equals(usuario)) {
                        oldUsuarioIdOfHistorialEquipoCollectionNewHistorialEquipo.getHistorialEquipoCollection().remove(historialEquipoCollectionNewHistorialEquipo);
                        oldUsuarioIdOfHistorialEquipoCollectionNewHistorialEquipo = em.merge(oldUsuarioIdOfHistorialEquipoCollectionNewHistorialEquipo);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = usuario.getUsuarioId();
                if (findUsuario(id) == null) {
                    throw new NonexistentEntityException("The usuario with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Usuario usuario;
            try {
                usuario = em.getReference(Usuario.class, id);
                usuario.getUsuarioId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The usuario with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<HistorialEquipo> historialEquipoCollectionOrphanCheck = usuario.getHistorialEquipoCollection();
            for (HistorialEquipo historialEquipoCollectionOrphanCheckHistorialEquipo : historialEquipoCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Usuario (" + usuario + ") cannot be destroyed since the HistorialEquipo " + historialEquipoCollectionOrphanCheckHistorialEquipo + " in its historialEquipoCollection field has a non-nullable usuarioId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Empleado rutEmpleado = usuario.getRutEmpleado();
            if (rutEmpleado != null) {
                rutEmpleado.getUsuarioCollection().remove(usuario);
                rutEmpleado = em.merge(rutEmpleado);
            }
            em.remove(usuario);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Usuario> findUsuarioEntities() {
        return findUsuarioEntities(true, -1, -1);
    }

    public List<Usuario> findUsuarioEntities(int maxResults, int firstResult) {
        return findUsuarioEntities(false, maxResults, firstResult);
    }

    private List<Usuario> findUsuarioEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Usuario.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Usuario findUsuario(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Usuario.class, id);
        } finally {
            em.close();
        }
    }

    public int getUsuarioCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Usuario> rt = cq.from(Usuario.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    /*Funcion agregada*/
    public Usuario findUsuarioByName(String nombreUsuario) {
        EntityManager em = getEntityManager();
        try {
            Query consulta = em.createNamedQuery("Usuario.findByNombreUsuario").setParameter("nombreUsuario", nombreUsuario);
            try{
                return (Usuario) consulta.getSingleResult();
            } catch (NoResultException ex) {
                return null;//Exepción para retornar null si no hay resultados
            }
        } finally {
            em.close();
        }
    }
    
}
