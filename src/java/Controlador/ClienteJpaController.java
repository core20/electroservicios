/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Controlador.exceptions.IllegalOrphanException;
import Controlador.exceptions.NonexistentEntityException;
import Controlador.exceptions.PreexistingEntityException;
import Controlador.exceptions.RollbackFailureException;
import Entidades.Cliente;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Entidades.Comuna;
import Entidades.Entrada;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.transaction.UserTransaction;

/**
 *
 * @author jorge
 */
public class ClienteJpaController implements Serializable {

    public ClienteJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Cliente cliente) throws PreexistingEntityException, RollbackFailureException, Exception {
        if (cliente.getEntradaCollection() == null) {
            cliente.setEntradaCollection(new ArrayList<Entrada>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Comuna codigoComuna = cliente.getCodigoComuna();
            if (codigoComuna != null) {
                codigoComuna = em.getReference(codigoComuna.getClass(), codigoComuna.getCodigoComuna());
                cliente.setCodigoComuna(codigoComuna);
            }
            Collection<Entrada> attachedEntradaCollection = new ArrayList<Entrada>();
            for (Entrada entradaCollectionEntradaToAttach : cliente.getEntradaCollection()) {
                entradaCollectionEntradaToAttach = em.getReference(entradaCollectionEntradaToAttach.getClass(), entradaCollectionEntradaToAttach.getEntradaId());
                attachedEntradaCollection.add(entradaCollectionEntradaToAttach);
            }
            cliente.setEntradaCollection(attachedEntradaCollection);
            em.persist(cliente);
            if (codigoComuna != null) {
                codigoComuna.getClienteCollection().add(cliente);
                codigoComuna = em.merge(codigoComuna);
            }
            for (Entrada entradaCollectionEntrada : cliente.getEntradaCollection()) {
                Cliente oldRutClienteOfEntradaCollectionEntrada = entradaCollectionEntrada.getRutCliente();
                entradaCollectionEntrada.setRutCliente(cliente);
                entradaCollectionEntrada = em.merge(entradaCollectionEntrada);
                if (oldRutClienteOfEntradaCollectionEntrada != null) {
                    oldRutClienteOfEntradaCollectionEntrada.getEntradaCollection().remove(entradaCollectionEntrada);
                    oldRutClienteOfEntradaCollectionEntrada = em.merge(oldRutClienteOfEntradaCollectionEntrada);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findCliente(cliente.getRut()) != null) {
                throw new PreexistingEntityException("Cliente " + cliente + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Cliente cliente) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Cliente persistentCliente = em.find(Cliente.class, cliente.getRut());
            Comuna codigoComunaOld = persistentCliente.getCodigoComuna();
            Comuna codigoComunaNew = cliente.getCodigoComuna();
            Collection<Entrada> entradaCollectionOld = persistentCliente.getEntradaCollection();
            Collection<Entrada> entradaCollectionNew = cliente.getEntradaCollection();
            List<String> illegalOrphanMessages = null;
            for (Entrada entradaCollectionOldEntrada : entradaCollectionOld) {
                if (!entradaCollectionNew.contains(entradaCollectionOldEntrada)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Entrada " + entradaCollectionOldEntrada + " since its rutCliente field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (codigoComunaNew != null) {
                codigoComunaNew = em.getReference(codigoComunaNew.getClass(), codigoComunaNew.getCodigoComuna());
                cliente.setCodigoComuna(codigoComunaNew);
            }
            Collection<Entrada> attachedEntradaCollectionNew = new ArrayList<Entrada>();
            for (Entrada entradaCollectionNewEntradaToAttach : entradaCollectionNew) {
                entradaCollectionNewEntradaToAttach = em.getReference(entradaCollectionNewEntradaToAttach.getClass(), entradaCollectionNewEntradaToAttach.getEntradaId());
                attachedEntradaCollectionNew.add(entradaCollectionNewEntradaToAttach);
            }
            entradaCollectionNew = attachedEntradaCollectionNew;
            cliente.setEntradaCollection(entradaCollectionNew);
            cliente = em.merge(cliente);
            if (codigoComunaOld != null && !codigoComunaOld.equals(codigoComunaNew)) {
                codigoComunaOld.getClienteCollection().remove(cliente);
                codigoComunaOld = em.merge(codigoComunaOld);
            }
            if (codigoComunaNew != null && !codigoComunaNew.equals(codigoComunaOld)) {
                codigoComunaNew.getClienteCollection().add(cliente);
                codigoComunaNew = em.merge(codigoComunaNew);
            }
            for (Entrada entradaCollectionNewEntrada : entradaCollectionNew) {
                if (!entradaCollectionOld.contains(entradaCollectionNewEntrada)) {
                    Cliente oldRutClienteOfEntradaCollectionNewEntrada = entradaCollectionNewEntrada.getRutCliente();
                    entradaCollectionNewEntrada.setRutCliente(cliente);
                    entradaCollectionNewEntrada = em.merge(entradaCollectionNewEntrada);
                    if (oldRutClienteOfEntradaCollectionNewEntrada != null && !oldRutClienteOfEntradaCollectionNewEntrada.equals(cliente)) {
                        oldRutClienteOfEntradaCollectionNewEntrada.getEntradaCollection().remove(entradaCollectionNewEntrada);
                        oldRutClienteOfEntradaCollectionNewEntrada = em.merge(oldRutClienteOfEntradaCollectionNewEntrada);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = cliente.getRut();
                if (findCliente(id) == null) {
                    throw new NonexistentEntityException("The cliente with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Cliente cliente;
            try {
                cliente = em.getReference(Cliente.class, id);
                cliente.getRut();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The cliente with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Entrada> entradaCollectionOrphanCheck = cliente.getEntradaCollection();
            for (Entrada entradaCollectionOrphanCheckEntrada : entradaCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Cliente (" + cliente + ") cannot be destroyed since the Entrada " + entradaCollectionOrphanCheckEntrada + " in its entradaCollection field has a non-nullable rutCliente field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Comuna codigoComuna = cliente.getCodigoComuna();
            if (codigoComuna != null) {
                codigoComuna.getClienteCollection().remove(cliente);
                codigoComuna = em.merge(codigoComuna);
            }
            em.remove(cliente);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Cliente> findClienteEntities() {
        return findClienteEntities(true, -1, -1);
    }

    public List<Cliente> findClienteEntities(int maxResults, int firstResult) {
        return findClienteEntities(false, maxResults, firstResult);
    }

    private List<Cliente> findClienteEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Cliente.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Cliente findCliente(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Cliente.class, id);
        } finally {
            em.close();
        }
    }

    public int getClienteCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Cliente> rt = cq.from(Cliente.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    /*Funcion agregada*/
    public Cliente findClienteByName(String nombreCliente) {
        EntityManager em = getEntityManager();
        try {
            Query consulta = em.createNamedQuery("Usuario.findByNombreCliente").setParameter("nombreCliente", nombreCliente);
            try {
                return (Cliente) consulta.getSingleResult();
            } catch (NoResultException ex) {
                return null;//Exepción para retornar null si no hay resultado
            }
        } finally {
            em.close();
        }
    }
}
