/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Controlador.exceptions.IllegalOrphanException;
import Controlador.exceptions.NonexistentEntityException;
import Controlador.exceptions.PreexistingEntityException;
import Controlador.exceptions.RollbackFailureException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Entidades.Provincia;
import Entidades.Cliente;
import Entidades.Comuna;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.transaction.UserTransaction;

/**
 *
 * @author jorge
 */
public class ComunaJpaController implements Serializable {

    public ComunaJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Comuna comuna) throws PreexistingEntityException, RollbackFailureException, Exception {
        if (comuna.getClienteCollection() == null) {
            comuna.setClienteCollection(new ArrayList<Cliente>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Provincia codigoProvincia = comuna.getCodigoProvincia();
            if (codigoProvincia != null) {
                codigoProvincia = em.getReference(codigoProvincia.getClass(), codigoProvincia.getCodigoProvincia());
                comuna.setCodigoProvincia(codigoProvincia);
            }
            Collection<Cliente> attachedClienteCollection = new ArrayList<Cliente>();
            for (Cliente clienteCollectionClienteToAttach : comuna.getClienteCollection()) {
                clienteCollectionClienteToAttach = em.getReference(clienteCollectionClienteToAttach.getClass(), clienteCollectionClienteToAttach.getRut());
                attachedClienteCollection.add(clienteCollectionClienteToAttach);
            }
            comuna.setClienteCollection(attachedClienteCollection);
            em.persist(comuna);
            if (codigoProvincia != null) {
                codigoProvincia.getComunaCollection().add(comuna);
                codigoProvincia = em.merge(codigoProvincia);
            }
            for (Cliente clienteCollectionCliente : comuna.getClienteCollection()) {
                Comuna oldCodigoComunaOfClienteCollectionCliente = clienteCollectionCliente.getCodigoComuna();
                clienteCollectionCliente.setCodigoComuna(comuna);
                clienteCollectionCliente = em.merge(clienteCollectionCliente);
                if (oldCodigoComunaOfClienteCollectionCliente != null) {
                    oldCodigoComunaOfClienteCollectionCliente.getClienteCollection().remove(clienteCollectionCliente);
                    oldCodigoComunaOfClienteCollectionCliente = em.merge(oldCodigoComunaOfClienteCollectionCliente);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findComuna(comuna.getCodigoComuna()) != null) {
                throw new PreexistingEntityException("Comuna " + comuna + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Comuna comuna) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Comuna persistentComuna = em.find(Comuna.class, comuna.getCodigoComuna());
            Provincia codigoProvinciaOld = persistentComuna.getCodigoProvincia();
            Provincia codigoProvinciaNew = comuna.getCodigoProvincia();
            Collection<Cliente> clienteCollectionOld = persistentComuna.getClienteCollection();
            Collection<Cliente> clienteCollectionNew = comuna.getClienteCollection();
            List<String> illegalOrphanMessages = null;
            for (Cliente clienteCollectionOldCliente : clienteCollectionOld) {
                if (!clienteCollectionNew.contains(clienteCollectionOldCliente)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Cliente " + clienteCollectionOldCliente + " since its codigoComuna field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (codigoProvinciaNew != null) {
                codigoProvinciaNew = em.getReference(codigoProvinciaNew.getClass(), codigoProvinciaNew.getCodigoProvincia());
                comuna.setCodigoProvincia(codigoProvinciaNew);
            }
            Collection<Cliente> attachedClienteCollectionNew = new ArrayList<Cliente>();
            for (Cliente clienteCollectionNewClienteToAttach : clienteCollectionNew) {
                clienteCollectionNewClienteToAttach = em.getReference(clienteCollectionNewClienteToAttach.getClass(), clienteCollectionNewClienteToAttach.getRut());
                attachedClienteCollectionNew.add(clienteCollectionNewClienteToAttach);
            }
            clienteCollectionNew = attachedClienteCollectionNew;
            comuna.setClienteCollection(clienteCollectionNew);
            comuna = em.merge(comuna);
            if (codigoProvinciaOld != null && !codigoProvinciaOld.equals(codigoProvinciaNew)) {
                codigoProvinciaOld.getComunaCollection().remove(comuna);
                codigoProvinciaOld = em.merge(codigoProvinciaOld);
            }
            if (codigoProvinciaNew != null && !codigoProvinciaNew.equals(codigoProvinciaOld)) {
                codigoProvinciaNew.getComunaCollection().add(comuna);
                codigoProvinciaNew = em.merge(codigoProvinciaNew);
            }
            for (Cliente clienteCollectionNewCliente : clienteCollectionNew) {
                if (!clienteCollectionOld.contains(clienteCollectionNewCliente)) {
                    Comuna oldCodigoComunaOfClienteCollectionNewCliente = clienteCollectionNewCliente.getCodigoComuna();
                    clienteCollectionNewCliente.setCodigoComuna(comuna);
                    clienteCollectionNewCliente = em.merge(clienteCollectionNewCliente);
                    if (oldCodigoComunaOfClienteCollectionNewCliente != null && !oldCodigoComunaOfClienteCollectionNewCliente.equals(comuna)) {
                        oldCodigoComunaOfClienteCollectionNewCliente.getClienteCollection().remove(clienteCollectionNewCliente);
                        oldCodigoComunaOfClienteCollectionNewCliente = em.merge(oldCodigoComunaOfClienteCollectionNewCliente);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = comuna.getCodigoComuna();
                if (findComuna(id) == null) {
                    throw new NonexistentEntityException("The comuna with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Comuna comuna;
            try {
                comuna = em.getReference(Comuna.class, id);
                comuna.getCodigoComuna();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The comuna with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Cliente> clienteCollectionOrphanCheck = comuna.getClienteCollection();
            for (Cliente clienteCollectionOrphanCheckCliente : clienteCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Comuna (" + comuna + ") cannot be destroyed since the Cliente " + clienteCollectionOrphanCheckCliente + " in its clienteCollection field has a non-nullable codigoComuna field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Provincia codigoProvincia = comuna.getCodigoProvincia();
            if (codigoProvincia != null) {
                codigoProvincia.getComunaCollection().remove(comuna);
                codigoProvincia = em.merge(codigoProvincia);
            }
            em.remove(comuna);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Comuna> findComunaEntities() {
        return findComunaEntities(true, -1, -1);
    }

    public List<Comuna> findComunaEntities(int maxResults, int firstResult) {
        return findComunaEntities(false, maxResults, firstResult);
    }

    private List<Comuna> findComunaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Comuna.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Comuna findComuna(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Comuna.class, id);
        } finally {
            em.close();
        }
    }

    public int getComunaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Comuna> rt = cq.from(Comuna.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    /*Funcion agregada*/
    public List<Provincia> findComunaEntitiesByCodigoProvincia(Provincia codigoProvincia) {
        EntityManager em = getEntityManager();
        try {
            Query consulta = em.createNamedQuery("Comuna.findByCodigoProvincia").setParameter("codigoProvincia", codigoProvincia);
            try{
                return consulta.getResultList();
            } catch (NoResultException ex) {
                return null;//Exepción para retornar null si no hay resultados
            }
        } finally {
            em.close();
        }
    }
    
}
