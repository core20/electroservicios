/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Controlador.exceptions.NonexistentEntityException;
import Controlador.exceptions.PreexistingEntityException;
import Controlador.exceptions.RollbackFailureException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Entidades.Usuario;
import Entidades.EstadoActualizacion;
import Entidades.Entrada;
import Entidades.HistorialEquipo;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

/**
 *
 * @author jorge
 */
public class HistorialEquipoJpaController implements Serializable {

    public HistorialEquipoJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(HistorialEquipo historialEquipo) throws PreexistingEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Usuario usuarioId = historialEquipo.getUsuarioId();
            if (usuarioId != null) {
                usuarioId = em.getReference(usuarioId.getClass(), usuarioId.getUsuarioId());
                historialEquipo.setUsuarioId(usuarioId);
            }
            EstadoActualizacion estadoActualizacionId = historialEquipo.getEstadoActualizacionId();
            if (estadoActualizacionId != null) {
                estadoActualizacionId = em.getReference(estadoActualizacionId.getClass(), estadoActualizacionId.getActualizacionId());
                historialEquipo.setEstadoActualizacionId(estadoActualizacionId);
            }
            Entrada entradaId = historialEquipo.getEntradaId();
            if (entradaId != null) {
                entradaId = em.getReference(entradaId.getClass(), entradaId.getEntradaId());
                historialEquipo.setEntradaId(entradaId);
            }
            em.persist(historialEquipo);
            if (usuarioId != null) {
                usuarioId.getHistorialEquipoCollection().add(historialEquipo);
                usuarioId = em.merge(usuarioId);
            }
            if (estadoActualizacionId != null) {
                estadoActualizacionId.getHistorialEquipoCollection().add(historialEquipo);
                estadoActualizacionId = em.merge(estadoActualizacionId);
            }
            if (entradaId != null) {
                entradaId.getHistorialEquipoCollection().add(historialEquipo);
                entradaId = em.merge(entradaId);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findHistorialEquipo(historialEquipo.getHistorialEquipoId()) != null) {
                throw new PreexistingEntityException("HistorialEquipo " + historialEquipo + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(HistorialEquipo historialEquipo) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            HistorialEquipo persistentHistorialEquipo = em.find(HistorialEquipo.class, historialEquipo.getHistorialEquipoId());
            Usuario usuarioIdOld = persistentHistorialEquipo.getUsuarioId();
            Usuario usuarioIdNew = historialEquipo.getUsuarioId();
            EstadoActualizacion estadoActualizacionIdOld = persistentHistorialEquipo.getEstadoActualizacionId();
            EstadoActualizacion estadoActualizacionIdNew = historialEquipo.getEstadoActualizacionId();
            Entrada entradaIdOld = persistentHistorialEquipo.getEntradaId();
            Entrada entradaIdNew = historialEquipo.getEntradaId();
            if (usuarioIdNew != null) {
                usuarioIdNew = em.getReference(usuarioIdNew.getClass(), usuarioIdNew.getUsuarioId());
                historialEquipo.setUsuarioId(usuarioIdNew);
            }
            if (estadoActualizacionIdNew != null) {
                estadoActualizacionIdNew = em.getReference(estadoActualizacionIdNew.getClass(), estadoActualizacionIdNew.getActualizacionId());
                historialEquipo.setEstadoActualizacionId(estadoActualizacionIdNew);
            }
            if (entradaIdNew != null) {
                entradaIdNew = em.getReference(entradaIdNew.getClass(), entradaIdNew.getEntradaId());
                historialEquipo.setEntradaId(entradaIdNew);
            }
            historialEquipo = em.merge(historialEquipo);
            if (usuarioIdOld != null && !usuarioIdOld.equals(usuarioIdNew)) {
                usuarioIdOld.getHistorialEquipoCollection().remove(historialEquipo);
                usuarioIdOld = em.merge(usuarioIdOld);
            }
            if (usuarioIdNew != null && !usuarioIdNew.equals(usuarioIdOld)) {
                usuarioIdNew.getHistorialEquipoCollection().add(historialEquipo);
                usuarioIdNew = em.merge(usuarioIdNew);
            }
            if (estadoActualizacionIdOld != null && !estadoActualizacionIdOld.equals(estadoActualizacionIdNew)) {
                estadoActualizacionIdOld.getHistorialEquipoCollection().remove(historialEquipo);
                estadoActualizacionIdOld = em.merge(estadoActualizacionIdOld);
            }
            if (estadoActualizacionIdNew != null && !estadoActualizacionIdNew.equals(estadoActualizacionIdOld)) {
                estadoActualizacionIdNew.getHistorialEquipoCollection().add(historialEquipo);
                estadoActualizacionIdNew = em.merge(estadoActualizacionIdNew);
            }
            if (entradaIdOld != null && !entradaIdOld.equals(entradaIdNew)) {
                entradaIdOld.getHistorialEquipoCollection().remove(historialEquipo);
                entradaIdOld = em.merge(entradaIdOld);
            }
            if (entradaIdNew != null && !entradaIdNew.equals(entradaIdOld)) {
                entradaIdNew.getHistorialEquipoCollection().add(historialEquipo);
                entradaIdNew = em.merge(entradaIdNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = historialEquipo.getHistorialEquipoId();
                if (findHistorialEquipo(id) == null) {
                    throw new NonexistentEntityException("The historialEquipo with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            HistorialEquipo historialEquipo;
            try {
                historialEquipo = em.getReference(HistorialEquipo.class, id);
                historialEquipo.getHistorialEquipoId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The historialEquipo with id " + id + " no longer exists.", enfe);
            }
            Usuario usuarioId = historialEquipo.getUsuarioId();
            if (usuarioId != null) {
                usuarioId.getHistorialEquipoCollection().remove(historialEquipo);
                usuarioId = em.merge(usuarioId);
            }
            EstadoActualizacion estadoActualizacionId = historialEquipo.getEstadoActualizacionId();
            if (estadoActualizacionId != null) {
                estadoActualizacionId.getHistorialEquipoCollection().remove(historialEquipo);
                estadoActualizacionId = em.merge(estadoActualizacionId);
            }
            Entrada entradaId = historialEquipo.getEntradaId();
            if (entradaId != null) {
                entradaId.getHistorialEquipoCollection().remove(historialEquipo);
                entradaId = em.merge(entradaId);
            }
            em.remove(historialEquipo);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<HistorialEquipo> findHistorialEquipoEntities() {
        return findHistorialEquipoEntities(true, -1, -1);
    }

    public List<HistorialEquipo> findHistorialEquipoEntities(int maxResults, int firstResult) {
        return findHistorialEquipoEntities(false, maxResults, firstResult);
    }

    private List<HistorialEquipo> findHistorialEquipoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(HistorialEquipo.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public HistorialEquipo findHistorialEquipo(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(HistorialEquipo.class, id);
        } finally {
            em.close();
        }
    }

    public int getHistorialEquipoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<HistorialEquipo> rt = cq.from(HistorialEquipo.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
