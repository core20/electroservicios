/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Entidades.EstadoActualizacion;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author alejandra
 */
@Stateless
public class EstadoActualizacionFacade extends AbstractFacade<EstadoActualizacion> {
    @PersistenceContext(unitName = "electroserviciosPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EstadoActualizacionFacade() {
        super(EstadoActualizacion.class);
    }
    
}
