<%@page contentType="text/html" pageEncoding="UTF-8"%>

<script type="text/javascript">
    $(document).ready( function (){
        if(hash === "dar-entrada"){
            startDarEntrada();
        }
        
        $('#formEntrada').submit(function(){
            //Si rut está incorrecto, no enviar formulario
            if( !verificarRut( $('#formEntrada [name="rut"]')[0] ) ){
                alert("Rut no válido");
                return false;
            }
            
            $.getJSON( "entradas?agregar&json="+JSON.parse(JSON.stringify(JSON.stringify({
                entradaId:null,
                serie:$('#formEntrada [name="serie"]').val(),
                modelo:$('#formEntrada [name="modelo"]').val(),
                fechaEntrada:1,
                fechaPrevistaEntrega:2,
                comentario:$('#formEntrada [name="problema"]').val(),
                accesorios:$('#formEntrada [name="accesorios"]').val()
            })))+"&rutCliente="+$('#formEntrada [name="rut"]').val().replace(/[^\dK]*/gi,'').substr( 0, $('#formEntrada [name="rut"]').val().replace(/[^\dK]*/gi,'').length-1 )
            +"&tipoEquipo="+$('#formEntrada [name="tipoEquipo"]').val()
            +"&marca="+$('#formEntrada [name="marca"]').val()
            ,
                function( respuesta ) {
                    if(respuesta){
                        if(respuesta['noError'] === true){
                            alert("Agregado correctamente");
                            $('#formEntrada')[0].reset();
                            startEntradas();
                        }else{
                            alert("Error!\nPosiblemente ya existe.");
                        }
                    }
            });
            
            return false;
        });
    });
    
    function startDarEntrada(){
        location.hash = "dar-entrada";
    }
    
    $('#formEntrada .buscarCliente').live('click',function(){
        verificarCliente();
    });
    $('#formEntrada [name="rut"]').live('blur change',function(){
        verificarCliente();
    });
    
    $('#formEntrada .agregarCliente').live('click',function(){
        startAgregarCliente();
    });
    
    function verificarCliente(){
        $('#formEntrada [name="nombre"]').val('');
        if( !verificarRut( $('#formEntrada [name="rut"]')[0] ) ){
            return false;
        }
        $('#formEntrada [name="nombre"]').nextAll('span').first().html("Buscando...");
        $.getJSON( "clientes?encontrar&rut="+( $('#formEntrada [name="rut"]').val().replace(/[^\dK]*/gi,'').substr( 0, $('#formEntrada [name="rut"]').val().replace(/[^\dK]*/gi,'').length-1 ) )
        , function( cliente ) {
            if(cliente){
                $('#formEntrada [name="nombre"]').val( cliente.nombres+" "+cliente.primerApellido+" "+cliente.segundoApellido );
                $('#formEntrada [name="nombre"]').nextAll('span').first().html("\u263a Cliente existe");
            }else{
                $('#formEntrada [name="nombre"]').val('');
                $('#formEntrada [name="nombre"]').nextAll('span').first().html("\u00D7 Cliente no existe");
            }
        });
    }
</script>

<fieldset>
    <legend>Dar Entrada</legend>
    <form id="formEntrada" name="formEntrada" action="" method="POST" enctype="multipart/form-data">
        <p>
            <label for="cliente_rut">Rut Cliente:</label>
            <input maxlength="15" type="text" name="rut" id="cliente_rut" placeholder="Rut" required=""/>
            <button type="button" class="buscarCliente">Buscar</button>
            <button type="button" class="agregarCliente noTargetScroll">Nuevo</button>
            <span class="alertRut"></span>
        </p>
        <p>
            <label for="nombre">Nombre:</label>
            <input style="color: #3C3C3C;" maxlength="100" type="text" name="nombre" id="nombre" value="" placeholder="(no definido)" disabled="disabled"/>
            <span class="alertNombre"></span>
        </p>
        
        <hr>
        
        <p>
            <label for="serie">Número de Serie:</label>
            <input maxlength="100" type="text" name="serie" id="serie" placeholder="Serie" required=""/>
        </p>
        <p>
            <label for="modelo">Modelo:</label>
            <input maxlength="100" type="text" name="modelo" id="modelo" placeholder="Modelo" required=""/>
        </p>
        <p>
            <label for="tipoEquipo">Tipo Equipo:</label>
            <select name="tipoEquipo" id="tipoEquipo">
                <option value=""></option>
                <option value="1">TV</option>
                <option value="2">Radio</option>
            </select>
        </p>
        <p>
            <label for="marca">Marca:</label>
            <select name="marca" id="marca">
                <option value=""></option>
                <option value="1">Sony</option>
                <option value="2">LG</option>
            </select>
        </p>
        
        <hr>
        
        <p>
            <label for="problema">Problema:</label>
            <textarea name="problema" id="problema" required=""></textarea>
        </p><p>
            <label for="accesorios">Accesorios:</label>
            <textarea name="accesorios" id="accesorios" required=""></textarea>
        </p>
        <p>
            <button type="submit">Dar Entrada</button>
            <button id="btnReset" type="reset">Limpiar Formulario</button>
        </p>
    </form>
</fieldset>
