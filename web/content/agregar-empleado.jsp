<%-- 
    Document   : agregar-empleado
    Created on : 20-10-2013, 07:47:31 PM
    Author     : jorge
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<script type="text/javascript">
    $(document).ready(function() {
        $('#formEmpleado').submit(function(){
            //Si rut está incorrecto, no enviar formulario
            if( !verificarRut( $('#formEmpleado [name="rut"]')[0] ) ){
                alert("Rut no válido");
                return false;
            }
            $.getJSON( "empleados?agregar&json="+JSON.parse(JSON.stringify(JSON.stringify({
                rut:$('#formEmpleado [name="rut"]').val().replace(/[^\dK]*/gi,'').substr( 0, $('#formEmpleado [name="rut"]').val().replace(/[^\dK]*/gi,'').length-1 ),
                rutVerif:$('#formEmpleado [name="rut"]').val().substr(-1),
                nombres:$('#formEmpleado [name="nombres"]').val(),
                primerApellido:$('#formEmpleado [name="primer_apellido"]').val(),
                segundoApellido:$('#formEmpleado [name="segundo_apellido"]').val()
            }))),
                function( respuesta ) {
                    if(respuesta){
                        if(respuesta['noError'] === true){
                            alert("Agregado correctamente");
                            $('#formEmpleado')[0].reset();
                            startEmpleados();
                        }else{
                            alert("Error!\nPosiblemente ya existe.");
                        }
                    }
            });
            
            return false;
        });
    });
    
    
    
    $('form [name="rut"]').live('mousedown mouseover keyup blur change',function(){
        formatRut(this);
        verificarRut(this);
    });
    
    function formatRut(input){
        var value = input.value.replace(/[^\dK]*|\-/gi,'');//Se reemplazará 2 veces, necesario para no permitir . al final ni guiones
        var dig=value.substr(value.length-1,1).replace(/[^\dK]*/gi,'');//Problemas con (-1) en IE
        var num=value.substr(0,value.length-1).replace(/[^\d]*/g,'');

        //if(!isNaN(num)) Borrado, para que vacíe también si no es caracter
        num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
        num = num.split('').reverse().join('').replace(/^[\.]/,'');
        input.value = ((num.length>0)?(num+'-'):'')+dig.toUpperCase();
    }

    function verificarRut(input){
        var value = input.value.replace(/[^\dK]*/gi,'');
        var dig=value.substr(value.length-1,1);//Problemas con (-1) en IE
        var num=value.substr(0,value.length-1);
        var digito, modulo;

        var suma = 0;
        for(var i=0,a=2,temprut=num; num.length>i; temprut /= 10,i++,a++){
            modulo = parseInt(temprut%10);
            if(a>7){
                a = 2;
            }
            suma += modulo*a;
        }

        var tempdigito = 11-(suma%11);
        if(tempdigito<10){
            digito = tempdigito;
        }else{
            if(tempdigito === 11){
                digito = 0;
            }else{
                if(tempdigito === 10){
                    digito = "K";
                }
            }
        }

        if(dig.toUpperCase() === digito.toString() && input.value !== ''){
            $(input).nextAll('span').first().html("\u263a Rut correcto");
            //alertRut.className = "alertTrue";
            //alertRut.innerHTML = "\u263a Rut correcto";
            return true;
        }else{
            $(input).nextAll('span').first().html("\u00D7 Rut incorrecto");
            //alertRut.className = "alertFalse";
            //alertRut.innerHTML = "\u00D7 Rut incorrecto";
            return false;
        }
    }
</script>

<fieldset>
    <legend>Agregar Empleado</legend>
    <form id="formEmpleado" name="formEmpleado" action="" method="POST" enctype="multipart/form-data">
        <p>
            <label for="empleado_rut">Rut:</label>
            <input maxlength="15" type="text" name="rut" id="empleado_rut" placeholder="Rut" required=""/>
            <span class="alertRut"></span>
        </p>
        <p>
            <label for="nombres">Nombres:</label>
            <input maxlength="100" type="text" name="nombres" id="nombres" placeholder="Nombres" required=""/>
        </p>
        <p>
            <label for="primer_apellido">Apellido Paterno:</label>
            <input maxlength="100" type="text" name="primer_apellido" id="primer_apellido" placeholder="Primer Apellido" required=""/>
        </p>
        <p>
            <label for="segundo_apellido">Apellido Materno:</label>
            <input maxlength="100" type="text" name="segundo_apellido" id="segundo_apellido" placeholder="Segundo Apellido" required=""/>
        </p>
        <p>
            <button type="submit">Registrar Empleado</button>
            <button id="btnReset" type="reset">Limpiar Formulario</button>
        </p>
</form>
</fieldset>
