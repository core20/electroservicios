<%-- 
    Document   : agregar-tipoEquipo
    Created on : 11-11-2013, 07:47:31 PM
    Author     : Manuel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<script type="text/javascript">
    $(document).ready(function() {
        $('#formAgregarTipoEquipo').submit(function(){
            $.getJSON( "tipoEquipo?agregar&json="+JSON.parse(JSON.stringify(JSON.stringify({
                tipoId:1,
                nombre:$('#formAgregarTipoEquipo [name="nombre"]').val(),
                costoRevision:$('#formAgregarTipoEquipo [name="costo"]').val()
            }))),
                function( respuesta ) {
                    if(respuesta){
                        if(respuesta['noError'] === true){
                            alert("Agregado correctamente");
                            $('#formAgregarTipoEquipo')[0].reset();
                            startTipoEquipos();
                        }else{
                            alert("Error!\nPosiblemente ya existe.");
                        }
                    }
            });
            
            return false;
        });
    });
    
     
    
</script>

<fieldset>
    <legend>Agregar Tipo Equipo</legend>
    <form id="formAgregarTipoEquipo" name="formAgregarTipoEquipo" action="" method="POST" enctype="multipart/form-data">
        <p>
            <label for="nombre">Nombre:</label>
            <input maxlength="100" type="text" name="nombre" id="nombre" placeholder="Nombre Tipo" required=""/>
        </p>
        <p>
            <label for="nombre">Costo:</label>
            <input maxlength="100" type="text" name="costo" id="nombre" placeholder="Costo" required=""/>
        </p>
        <p>
            <button type="submit">Agregar Tipo</button>
            <button id="btnReset" type="reset">Limpiar Formulario</button>
        </p>
</form>
</fieldset>
