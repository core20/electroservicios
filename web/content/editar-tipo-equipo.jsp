<%-- 
    Document   : editar-tipo-equipo
    Created on : 13-11-2013, 09:59:11 AM
    Author     : manuel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<script type="text/javascript">
    $(document).ready(function() {
        $('#formEditarTipoEquipo').submit(function(){

            $.getJSON( "tipoEquipo?editar&json="+JSON.parse(JSON.stringify(JSON.stringify({
                tipoId:$('#formEditarTipoEquipo [name="tipoId"]').val(),
                nombre:$('#formEditarTipoEquipo [name="nombre"]').val(),
                costoRevision:$('#formEditarTipoEquipo [name="costo"]').val()
            }))),
                function( respuesta ) {
                    if(respuesta){
                        if(respuesta['noError'] === true){
                            alert("Editado correctamente");
                            $('#formEditarTipoEquipo')[0].reset();
                            startTipoEquipos();
                        }else{
                            alert("Error!\nPosiblemente ya existe.");
                        }
                    }
            });
            
            return false;
        });
    });
    
</script>

<fieldset>
    <legend>Editar Tipo Equipo</legend>
    <form id="formEditarTipoEquipo" name="formEditarTipoEquipo" action="" method="POST" enctype="multipart/form-data">
        <p>
            <label for="nombre">Nombre:</label>
            <input maxlength="100" type="text" name="tipoId" id="tipoId" placeholder="Id tipo" hidden="hidden"/>
            <input maxlength="100" type="text" name="nombre" id="nombre" placeholder="Nombre Tipo" required=""/>
        </p>
        <p>
            <label for="costo">Costo:</label>
            <input maxlength="100" type="text" name="costo" id="costo" placeholder="Costo" required=""/>
        </p>
        <p>
            <button type="submit">Editar Tipo Equipo</button>
        </p>
</form>
</fieldset>
