<%-- 
    Document   : historial
    Created on : 13-11-2013, 01:52:10 PM
    Author     : ale
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<script type="text/javascript">
    //Sólo ejecutaba la última funcion, no pueden iniciarse múltiples windos.onload
    //window.onload = function(){
    $(document).ready( function (){
        if(hash === "agregar-historial"){
            startHistorial();
        }
        
        $('#formAgregarHistorial').submit(function(){
            $.getJSON( "historialEquipo?agregar&json="+JSON.parse(JSON.stringify(JSON.stringify({
                historialEquipoId:1,
                fecha:1,
                detalle:$('#formAgregarHistorial [name="detalle"]').val(),
                valor:$('#formAgregarHistorial [name="valor"]').val()
            })))+"&entradaId="+$('#formAgregarHistorial [name="entradaId"]').val()
            +"&estadoId="+$('#formAgregarHistorial [name="estado"]').val()
            ,
                function( respuesta ) {
                    if(respuesta){
                        if(respuesta['noError'] === true){
                            alert("Agregado correctamente");
                            $('#formEntrada')[0].reset();
                            startVerEntrada($('#formAgregarHistorial [name="entradaId"]').val());
                        }else{
                            alert("Error!\nPosiblemente ya existe.");
                        }
                    }
            });
            
            return false;
        });
    });
       
    function startHistorial(entrada){
        $('#formAgregarHistorial #entradaId').val(entrada);
        location.hash = "agregar-historial";
        findHistoriales(0);
        actual = 0;
    }
    
    function findHistoriales(start){
        $('#table-historiales tbody').html('<tr><td colspan="9" style="text-align:center;font-weight:bold;font-size:20px;">Cargando...</td></tr>');
        
        $.getJSON( "historiales?find&start="+start, function( hitoriales ) {
            $('#table-historiales tbody').html('');
            
            $.each(historiales, function() {
                $('#table-historiales tbody').append('<tr>\
                                                        <td>'+this.nombre+'<input value="'+this.actualizacionId+'" hidden="hidden"></td>\
                                                        <td><button type="button" class="editar">Agregar</button></td>\
                                                        </tr>');
            });
        });
    }
    
</script>

<fieldset>
    <legend>Agregar Historial</legend>
    <form id="formAgregarHistorial" name="formAgregarHistorial" action="" method="POST" enctype="multipart/form-data">
        
        <p>
            <input maxlength="100" type="text" name="entradaId" id="entradaId" placeholder="Id Entrada" hidden="hidden"/>
            <label for="estado"></label>
            <select name="estado" id="estado">
                <option value="1">Recibido</option>
                <option value="2">Despachado</option>
                <option value="2">Reparado</option>
            </select>
        </p>
        <p>
            <label for="detalle">Detalle:</label>
            <textarea name="detalle" id="detalle" required=""></textarea>
        </p>
        <p>
            <label for="valor">Valor:</label>
            <input maxlength="100" type="text" name="valor" id="valor" placeholder="Valor" required=""/>
        </p>
        <p>
            <button type="submit">Actualizar</button>
        </p>
</form>
</fieldset>