<%-- 
    Document   : actualizar-estado
    Created on : 13-11-2013, 12:49:18 PM
    Author     : ale
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<script type="text/javascript">
    $(document).ready(function() {
        $('#formAgregarEstado').submit(function(){
            $.getJSON( "estados?agregar&json="+JSON.parse(JSON.stringify(JSON.stringify({
                actualizacionId:1,
                nombre:$('#formAgregarEstado [name="nombre"]').val(),
                actualizacionTipo:3
            }))),
                function( respuesta ) {
                    if(respuesta){
                        if(respuesta['noError'] === true){
                            alert("Agregado correctamente");
                            $('#formAgregarEstado')[0].reset();
                            startEstados();
                        }else{
                            alert("Error!\nPosiblemente ya existe.");
                        }
                    }
            });
            
            return false;
        });
    });
    
     
    
</script>

<fieldset>
    <legend>Agregar Estado</legend>
    <form id="formAgregarEstado" name="formAgregarEstado" action="" method="POST" enctype="multipart/form-data">
        <p>
            <label for="nombre">Estado:</label>
            <input maxlength="100" type="text" name="nombre" id="nombre" placeholder="Estado" required=""/>
        </p>
            <button type="submit">Agregar Estado</button>
            <button id="btnReset" type="reset">Limpiar Formulario</button>
        </p>
</form>
</fieldset>
