<%-- 
    Document   : empleados
    Created on : 18-10-2013, 08:23:02 PM
    Author     : jorge
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<script type="text/javascript">
    $(document).ready(function() {
        if (hash === "empleados") {
            startEmpleados();
        }
    });

    $('#menu-empleados > a').live('click', function() {
        startEmpleados();
    });

    $('#div-empleados #table-empleados .eliminar').live('click', function() {
        delEmpleado($(this).parent().parent().find('td').html().replace(/[^\dK]*/gi, '').substr(0, $(this).parent().parent().find('td').html().replace(/[^\dK]*/gi, '').length - 1));
    });

    $('#div-empleados #table-empleados .editar').live('click', function() {
        editEmpleado($(this).parent().parent().find('td').html().replace(/[^\dK]*/gi, '').substr(0, $(this).parent().parent().find('td').html().replace(/[^\dK]*/gi, '').length - 1));
    });

    function startEmpleados() {
        location.hash = "empleados";
        findEmpleados(0);
    }

    function findEmpleados(start) {
        $('#table-empleados tbody').html('<tr><td colspan="4" style="text-align:center;font-weight:bold;font-size:20px;">Cargando...</td></tr>');
        /*getJSON de JQuery que carga URL listaBodegas que retorna el SELECT de las
         * Bodegas en formato JSON*/
        $.getJSON("empleados?find&start=" + start, function(empleados) {
            $('#table-empleados tbody').html('');
            /*Para cada contenido de bodega, recorrer y ejecutar*/
            $.each(empleados, function() {
                $('#table-empleados tbody').append('<tr>\
                                                        <td>' + this.rut + '-' + this.rutVerif + '</td>\
                                                        <td>' + this.nombres + ' ' + this.primerApellido + ' ' + this.segundoApellido + '</td>\
                                                        <td><button type="button" class="editar">Editar</button></td>\
                                                        <td><button type="button" class="eliminar">Eliminar</button></td>\
                                                    </tr>');
            });
        });
    }

    function delEmpleado(rut) {
        $.getJSON("empleados?delete&rut=" + rut, function(respuesta) {
            if (respuesta['noError'] === true) {
                alert("Eliminado!");
                startEmpleados();
            } else {
                alert("Error!\nNo se pudo eliminar.");
            }
        });
    }

    function editEmpleado(rut) {
        $.getJSON("empleados?encontrar&rut=" + rut, function(respuesta) {
            if (respuesta) {
                $('#formEditarEmpleado [name="rut"]').val(respuesta.rut + respuesta.rutVerif);
                $('#formEditarEmpleado [name="nombres"]').val(respuesta.nombres);
                $('#formEditarEmpleado [name="primer_apellido"]').val(respuesta.primerApellido);
                $('#formEditarEmpleado [name="segundo_apellido"]').val(respuesta.segundoApellido);
            } else {
                alert("Error!\nNo se pudo encontrar.");
            }
        });
        location.hash = "editar-empleado";
    }
</script>

<div id="div-empleados">
    <h2>Empleados</h2>
    
    <table id="table-empleados" class="entidades">
        <thead>
            <tr>
                <th><label>Rut</label></th>
                <th><label>Nombre</label></th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
