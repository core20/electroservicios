<%-- 
    Document   : clientes
    Created on : 18-10-2013, 08:23:31 PM
    Author     : jorge
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<script type="text/javascript">
    //Sólo ejecutaba la última funcion, no pueden iniciarse múltiples windos.onload
    //window.onload = function(){
    $(document).ready( function (){
        if(hash === "clientes"){
            startClientes();
        }
    });
    
    $('#menu-clientes > a').live('click',function(){
        startClientes();
    });
    
    actual = 0;
    $('#div-cliente .controlTabla .tablaSiguiente').live('click',function(){
        if($('#table-clientes tbody tr').length === 10){
            findClientes(actual+10);
            actual += 10;
        }
    });
    $('#div-cliente .controlTabla .tablaAnterior').live('click',function(){
        if(actual-10 > 0){
            findClientes(actual-10);
            actual -= 10;
        }else{
            findClientes(0);
            actual = 0;
        }
    });
    $('#div-cliente .controlTabla .tablaInicial').live('click',function(){
        findClientes(0);
        actual = 0;
    });
    
    $('#div-cliente #table-clientes .eliminar').live('click',function(){
        delCliente( $(this).parent().parent().find('td').html().replace(/[^\dK]*/gi,'').substr( 0, $(this).parent().parent().find('td').html().replace(/[^\dK]*/gi,'').length-1 ) );
    });
    
    function startClientes(){
        location.hash = "clientes";
        findClientes(0);
        actual = 0;
    }
    
    function findClientes(start){
        $('#table-clientes tbody').html('<tr><td colspan="9" style="text-align:center;font-weight:bold;font-size:20px;">Cargando...</td></tr>');
        /*getJSON de JQuery que carga URL listaBodegas que retorna el SELECT de las
         * Bodegas en formato JSON*/
        $.getJSON( "clientes?find&start="+start, function( clientes ) {
            $('#table-clientes tbody').html('');
            /*Para cada contenido de bodega, recorrer y ejecutar*/
            $.each(clientes, function() {
                $('#table-clientes tbody').append('<tr>\
                                                        <td>'+this.rut+'-'+this.rutVerif+'</td>\
                                                        <td>'+this.nombres+' '+this.primerApellido+' '+this.segundoApellido+'</td>\
                                                        <td>'+this.direccion+'</td>\
                                                        <td>'+this.fono1+'</td>\
                                                        <td>'+this.fono2+'</td>\
                                                        <td>'+this.email+'</td>\
                                                        <td>'+this.clienteTipo+'</td>\
                                                        <td><button type="button" class="editar">Editar</button></td>\
                                                        <td><button type="button" class="eliminar">Eliminar</button></td>\
                                                    </tr>');
            });
        });
    }
        
    $('#div-cliente #table-clientes .editar').live('click',function(){
        editCliente( $(this).parent().parent().find('td').html().replace(/[^\dK]*/gi,'').substr( 0, $(this).parent().parent().find('td').html().replace(/[^\dK]*/gi,'').length-1 ) );
    });
    
    function editCliente(rut){
        $.getJSON( "clientes?encontrar&rut="+rut, function( respuesta ) {
            if(respuesta){
                $('#formEditarCliente [name="rut"]').val(respuesta.rut+respuesta.rutVerif);
                $('#formEditarCliente [name="cliente_tipo"]').val(respuesta.clienteTipo);
                $('#formEditarCliente [name="nombres"]').val(respuesta.nombres);
                $('#formEditarCliente [name="primer_apellido"]').val(respuesta.primerApellido);
                $('#formEditarCliente [name="segundo_apellido"]').val(respuesta.segundoApellido);
                $('#formEditarCliente [name="direccion"]').val(respuesta.direccion);
                $('#formEditarCliente [name="fono1"]').val(respuesta.fono1);
                $('#formEditarCliente [name="fono2"]').val(respuesta.fono2);
                $('#formEditarCliente [name="email"]').val(respuesta.email);
            }else{
                alert("Error!\nNo se pudo encontrar.");
            }
        });
        location.hash = "editar-clientes";
    }
    
    function delCliente(rut){
        $.getJSON( "clientes?delete&rut="+rut, function( respuesta ) {
            if(respuesta['noError'] === true){
                alert("Eliminado!");
                startClientes();
            }else{
                alert("Error!\nNo se pudo eliminar.");
            }
        });
    }
</script>

<div id="div-cliente">
    <h2>Clientes</h2>
    <div class="controlTabla">
        <button class="tablaAnterior" type="button">Anterior</button>
        <button class="tablaInicial" type="button">Inicio</button>
        <button class="tablaSiguiente" type="button">Siguiente</button>
    </div>
    <table id="table-clientes" class="entidades">
        <thead>
            <tr>
                <th><label>Rut</label></th>
                <th><label>Nombre</label></th>
                <th><label>Dirección</label></th>
                <th><label>Fono1</label></th>
                <th><label>Fono2</label></th>
                <th><label>Email</label></th>
                <th><label>Tipo Cliente</label></th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>