<%-- 
    Document   : estados
    Created on : 13-11-2013, 01:52:10 PM
    Author     : ale
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<script type="text/javascript">
    //Sólo ejecutaba la última funcion, no pueden iniciarse múltiples windos.onload
    //window.onload = function(){
    $(document).ready( function (){
        if(hash === "estados"){
            startEstados();
        }
    });
    
    $('#menu-estados > a').live('click',function(){
        startEstados();
    });
    
    actual = 0;
    $('#div-estado .controlTabla .tablaSiguiente').live('click',function(){
        if($('#table-estados tbody tr').length === 10){
            findEstados(actual+10);
            actual += 10;
        }
    });
    $('#div-estado .controlTabla .tablaAnterior').live('click',function(){
        if(actual-10 > 0){
            findEstados(actual-10);
            actual -= 10;
        }else{
            findEstados(0);
            actual = 0;
        }
    });
    $('#div-estado .controlTabla .tablaInicial').live('click',function(){
        findEstados(0);
        actual = 0;
    });
    
    $('#div-estado #table-estados .eliminar').live('click',function(){
        delEstado( $(this).parent().parent().find('td').find('input').val() );
    });
    
    $('#div-estado #table-estados .editar').live('click',function(){
        editEstado( $(this).parent().parent().find('td').find('input').val() );
    });
    
    function startEstados(){
        location.hash = "estados";
        findEstados(0);
        actual = 0;
    }
    
    function findEstados(start){
        $('#table-estados tbody').html('<tr><td colspan="9" style="text-align:center;font-weight:bold;font-size:20px;">Cargando...</td></tr>');
        
        $.getJSON( "estados?find&start="+start, function( estados ) {
            $('#table-estados tbody').html('');
            
            $.each(estados, function() {
                $('#table-estados tbody').append('<tr>\
                                                        <td>'+this.nombre+'<input value="'+this.actualizacionId+'" hidden="hidden"></td>\
                                                        <td><button type="button" class="editar">Editar</button></td>\
                                                        <td><button type="button" class="eliminar">Eliminar</button></td>\
                                                    </tr>');
            });
        });
    }
    
    function editEstado(actualizacionId){
        $.getJSON( "estados?encontrar&id_estado="+actualizacionId, function( respuesta ) {
            if(respuesta){
                $('#formEditarEstado [name="actualizacionId"]').val(respuesta.actualizacionId);
                $('#formEditarEstado [name="nombre"]').val(respuesta.nombre);
                $('#formEditarEstado [name="actualizacionTipo"]').val(respuesta.actualizacionTipo);
            }else{
                alert("Error!\nNo se pudo encontrar.");
            }
        });
        location.hash = "editar-estado";
    }
    
    function delEstado(actualizacionId){
        $.getJSON( "estados?delete&estado_id="+actualizacionId, function( respuesta ) {
            if(respuesta['noError'] === true){
                alert("Eliminado!");
                startEstados();
            }else{
                alert("Error!\nNo se pudo eliminar.");
            }
        });
    }
</script>

<div id="div-estado">
    <h2>Estados</h2>
    <div class="controlTabla">
        <button class="tablaAnterior" type="button">Anterior</button>
        <button class="tablaInicial" type="button">Inicio</button>
        <button class="tablaSiguiente" type="button">Siguiente</button>
    </div>
    <table id="table-estados" class="entidades">
        <thead>
            <tr>
                <th><label>Nombre</label></th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>