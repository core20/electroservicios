<%-- 
    Document   : agregar-empleado
    Created on : 20-10-2013, 07:47:31 PM
    Author     : jorge
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<script type="text/javascript">
    $(document).ready(function() {
        $('#formAgregarMarca').submit(function(){
            $.getJSON( "marcas?agregar&json="+JSON.parse(JSON.stringify(JSON.stringify({
                marcaId:1,
                nombre:$('#formAgregarMarca [name="nombre"]').val()
            }))),
                function( respuesta ) {
                    if(respuesta){
                        if(respuesta['noError'] === true){
                            alert("Agregado correctamente");
                            $('#formAgregarMarca')[0].reset();
                            startMarcas();
                        }else{
                            alert("Error!\nPosiblemente ya existe.");
                        }
                    }
            });
            
            return false;
        });
    });
    
     
    
</script>

<fieldset>
    <legend>Agregar Marca</legend>
    <form id="formAgregarMarca" name="formAgregarMarca" action="" method="POST" enctype="multipart/form-data">
        <p>
            <label for="nombre">Nombre:</label>
            <input maxlength="100" type="text" name="nombre" id="nombre" placeholder="Nombre Marca" required=""/>
        </p>
            <button type="submit">Agregar Marca</button>
            <button id="btnReset" type="reset">Limpiar Formulario</button>
        </p>
</form>
</fieldset>
