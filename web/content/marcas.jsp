<%-- 
    Document   : marcas
    Created on : 18-10-2013, 08:23:31 PM
    Author     : jorge.

--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<script type="text/javascript">
    //Sólo ejecutaba la última funcion, no pueden iniciarse múltiples windos.onload
    //window.onload = function(){
    $(document).ready( function (){
        if(hash === "marcas"){
            startMarcas();
        }
    });
    
    $('#menu-marcas > a').live('click',function(){
        startMarcas();
    });
    
    actual = 0;
    $('#div-marca .controlTabla .tablaSiguiente').live('click',function(){
        if($('#table-marcas tbody tr').length === 10){
            findMarcas(actual+10);
            actual += 10;
        }
    });
    $('#div-marca .controlTabla .tablaAnterior').live('click',function(){
        if(actual-10 > 0){
            findMarcas(actual-10);
            actual -= 10;
        }else{
            findMarcas(0);
            actual = 0;
        }
    });
    $('#div-marca .controlTabla .tablaInicial').live('click',function(){
        findMarcas(0);
        actual = 0;
    });
    
    $('#div-marca #table-marcas .eliminar').live('click',function(){
        delMarca( $(this).parent().parent().find('td').find('input').val() );
    });
    
    $('#div-marca #table-marcas .editar').live('click',function(){
        editMarca( $(this).parent().parent().find('td').find('input').val() );
    });
    
    function startMarcas(){
        location.hash = "marcas";
        findMarcas(0);
        actual = 0;
    }
    
    function findMarcas(start){
        $('#table-marcas tbody').html('<tr><td colspan="9" style="text-align:center;font-weight:bold;font-size:20px;">Cargando...</td></tr>');
        /*getJSON de JQuery que carga URL listaBodegas que retorna el SELECT de las
         * Bodegas en formato JSON*/
        $.getJSON( "marcas?find&start="+start, function( marcas ) {
            $('#table-marcas tbody').html('');
            /*Para cada contenido de bodega, recorrer y ejecutar*/
            $.each(marcas, function() {
                $('#table-marcas tbody').append('<tr>\
                                                        <td>'+this.nombre+'<input value="'+this.marcaId+'" hidden="hidden">'+'</td>\
                                                        <td><button type="button" class="editar">Editar</button></td>\
                                                        <td><button type="button" class="eliminar">Eliminar</button></td>\
                                                    </tr>');
            });
        });
    }
    
    function editMarca(marcaId){
        $.getJSON( "marcas?encontrar&id_marca="+marcaId, function( respuesta ) {
            if(respuesta){
                $('#formEditarMarca [name="marcaId"]').val(respuesta.marcaId);
                $('#formEditarMarca [name="nombre"]').val(respuesta.nombre);
            }else{
                alert("Error!\nNo se pudo encontrar.");
            }
        });
        location.hash = "editar-marca";
    }
    
    function delMarca(marcaId){
        $.getJSON( "marcas?delete&marca_id="+marcaId, function( respuesta ) {
            if(respuesta['noError'] === true){
                alert("Eliminado!");
                startMarcas();
            }else{
                alert("Error!\nNo se pudo eliminar.");
            }
        });
    }
</script>

<div id="div-marca">
    <h2>Marcas</h2>
    <div class="controlTabla">
        <button class="tablaAnterior" type="button">Anterior</button>
        <button class="tablaInicial" type="button">Inicio</button>
        <button class="tablaSiguiente" type="button">Siguiente</button>
    </div>
    <table id="table-marcas" class="entidades">
        <thead>
            <tr>
                <th><label>Nombre</label></th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>