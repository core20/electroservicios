<%-- 
    Document   : editar-clientes
    Created on : 10-11-2013, 09:59:11 AM
    Author     : naxo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<script type="text/javascript">
    $(document).ready( function (){
        if(hash === "editar-clientes"){
            startEditarCliente();
        }
        
        $('#formEditarCliente').submit(function(){
            //Si rut está incorrecto, no enviar formulario
            if( !verificarRut( $('#formEditarCliente [name="rut"]')[0] ) ){
                alert("Rut no válido");
                return false;
            }
            //rut = $('#formEditarCliente [name="rut"]').val().replace(/[^\dK]*/gi,'');
            //rut_digverif = rut.substr(rut.length-1,1);
            //rut_numero = rut.substr(0,rut.length-1);//Problemas usando (-1) en IE
            $.getJSON( "clientes?editar&json="+JSON.parse(JSON.stringify(JSON.stringify({
                rut:$('#formEditarCliente [name="rut"]').val().replace(/[^\dK]*/gi,'').substr( 0, $('#formEditarCliente [name="rut"]').val().replace(/[^\dK]*/gi,'').length-1 ),
                rutVerif:$('#formEditarCliente [name="rut"]').val().substr(-1),
                nombres:$('#formEditarCliente [name="nombres"]').val(),
                primerApellido:$('#formEditarCliente [name="primer_apellido"]').val(),
                segundoApellido:$('#formEditarCliente [name="segundo_apellido"]').val(),
                direccion:$('#formEditarCliente [name="direccion"]').val(),
                fono1:$('#formEditarCliente [name="fono1"]').val(),
                fono2:$('#formEditarCliente [name="fono2"]').val(),
                email:$('#formEditarCliente [name="email"]').val(),
                clienteTipo:$('#formEditarCliente [name="cliente_tipo"]').val()
            })))+"&codigoComuna="+$('#formEditarCliente [name="codigo_comuna"]').val()
            ,
                function( respuesta ) {
                    if(respuesta){
                        if(respuesta['noError'] === true){
                            alert("Editado correctamente");
                            $('#formEditarCliente')[0].reset();
                            startClientes();
                        }else{
                            alert("Error!\nNo se a podido editar.");
                        }
                    }
            });
            
            return false;
        });
    });
    
    $('#div-cliente #table-clientes .editar').live('click',function(){
        startEditarCliente();
    });
    
    function startEditarCliente(){
        location.hash = "editar-clientes";
        findRegiones($('#formEditarCliente [name="codigo_region"]'));
        //findProvincias( $('#formEditarCliente [name="codigo_provincia"]') , $('#formEditarCliente [name="codigo_region"]')[0].value);
        //findComunas( $('#formEditarCliente [name="codigo_comuna"]') , $('#formEditarCliente [name="codigo_provincia"]')[0].value);
    }
    
    $('#formEditarCliente [name="codigo_region"]').live('change',function(){
        findProvincias( $('#formEditarCliente [name="codigo_provincia"]') , this.value);
        findComunas( $('#formEditarCliente [name="codigo_comuna"]') , $('#formEditarCliente [name="codigo_provincia"]')[0].value);
    });
    $('#formEditarCliente [name="codigo_provincia"]').live('mousedown mouseover keyup blur',function(){
        if($('#formEditarCliente [name="codigo_region"]').val() === ''){
            $(this).html('');
        }
    });
    
    $('#formEditarCliente [name="codigo_provincia"]').live('change',function(){
        findComunas( $('#formEditarCliente [name="codigo_comuna"]') , this.value);
    });
    $('#formEditarCliente [name="codigo_comuna"]').live('mousedown mouseover keyup blur',function(){
        if($('#formEditarCliente [name="codigo_provincia"]').val() === ''){
            $(this).html('');
        }
    });
</script>

<fieldset>
    <legend>Editar Cliente</legend>
    <form id="formEditarCliente" name="formEditarCliente" action="" method="POST" enctype="multipart/form-data">
        <p>
            <label for="cliente_tipo">Tipo de cliente:</label>
            <select name="cliente_tipo" id="cliente_tipo">
                <option value="1">Persona</option>
                <option value="2">Empresa</option>
            </select>
        </p>
        <p>
            <label for="cliente_rut">Rut:</label>
            <input style="color: #3C3C3C;" maxlength="15" type="text" name="rut" id="cliente_rut" placeholder="Rut" required="" disabled="disabled"/>
            <span class="alertRut"></span>
        </p>
        <p>
            <label for="nombres">Nombres:</label>
            <input maxlength="100" type="text" name="nombres" id="nombres" placeholder="Nombres" required=""/>
        </p>
        <p>
            <label for="primer_apellido">Apellido Paterno:</label>
            <input maxlength="100" type="text" name="primer_apellido" id="primer_apellido" placeholder="Primer Apellido"/>
        </p>
        <p>
            <label for="segundo_apellido">Apellido Materno:</label>
            <input maxlength="100" type="text" name="segundo_apellido" id="segundo_apellido" placeholder="Segundo Apellido"/>
        </p>
        <p>
            <label for="direccion">Dirección:</label>
            <input maxlength="100" type="text" name="direccion" id="direccion" placeholder="Dirección" required=""/>
        </p>
        <p>
            <label for="codigo_region">Región:</label>
            <select name="codigo_region" id="codigo_region"></select>
        </p>
        <p>
            <label for="codigo_provincia">Provincia:</label>
            <select name="codigo_provincia" id="codigo_provincia"></select>
        </p>
        <p>
            <label for="codigo_comuna">Comuna:</label>
            <select name="codigo_comuna" id="codigo_comuna"></select>
        </p>
        <p>
            <label for="fono1">Fono1:</label>
            <input maxlength="20" type="text" name="fono1" id="fono1" placeholder="Fono1" required=""/>
        </p>
        <p>
            <label for="fono2">Fono2:</label>
            <input maxlength="20" type="text" name="fono2" id="fono2" placeholder="Fono2" required=""/>
        </p>
        <p>
            <label for="email">E-Mail:</label>
            <input maxlength="100" type="text" name="email" id="email" placeholder="E-Mail" required=""/>
        </p>
        <p>
            <button type="submit">Editar Cliente</button>
        </p>
</form>
</fieldset>