<%-- 
    Document   : agregar-cliente
    Created on : 20-10-2013, 05:22:16 PM
    Author     : jorge
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<script type="text/javascript">
    $(document).ready( function (){
        if(hash === "agregar-cliente"){
            startAgregarCliente();
        }
        
        $('#formCliente').submit(function(){
            //Si rut está incorrecto, no enviar formulario
            if( !verificarRut( $('#formCliente [name="rut"]')[0] ) ){
                alert("Rut no válido");
                return false;
            }
            //rut = $('#formCliente [name="rut"]').val().replace(/[^\dK]*/gi,'');
            //rut_digverif = rut.substr(rut.length-1,1);
            //rut_numero = rut.substr(0,rut.length-1);//Problemas usando (-1) en IE
            $.getJSON( "clientes?agregar&json="+JSON.parse(JSON.stringify(JSON.stringify({
                rut:$('#formCliente [name="rut"]').val().replace(/[^\dK]*/gi,'').substr( 0, $('#formCliente [name="rut"]').val().replace(/[^\dK]*/gi,'').length-1 ),
                rutVerif:$('#formCliente [name="rut"]').val().substr(-1),
                nombres:$('#formCliente [name="nombres"]').val(),
                primerApellido:$('#formCliente [name="primer_apellido"]').val(),
                segundoApellido:$('#formCliente [name="segundo_apellido"]').val(),
                direccion:$('#formCliente [name="direccion"]').val(),
                fono1:$('#formCliente [name="fono1"]').val(),
                fono2:$('#formCliente [name="fono2"]').val(),
                email:$('#formCliente [name="email"]').val(),
                clienteTipo:$('#formCliente [name="cliente_tipo"]').val()
            })))+"&codigoComuna="+$('#formCliente [name="codigo_comuna"]').val()
            ,
                function( respuesta ) {
                    if(respuesta){
                        if(respuesta['noError'] === true){
                            alert("Agregado correctamente");
                            $('#formCliente')[0].reset();
                            startClientes();
                        }else{
                            alert("Error!\nPosiblemente ya existe.");
                        }
                    }
            });
            
            return false;
        });
    });
    
    $('#menu-clientes a[href="#agregar-cliente"]').live('click',function(){
        startAgregarCliente();
    });
    
    function startAgregarCliente(){
        location.hash = "agregar-cliente";
        findRegiones($('#formCliente [name="codigo_region"]'));
        //findProvincias( $('#formCliente [name="codigo_provincia"]') , $('#formCliente [name="codigo_region"]')[0].value);
        //findComunas( $('#formCliente [name="codigo_comuna"]') , $('#formCliente [name="codigo_provincia"]')[0].value);
    }
    
    $('#formCliente [name="codigo_region"]').live('change',function(){
        findProvincias( $('#formCliente [name="codigo_provincia"]') , this.value);
        findComunas( $('#formCliente [name="codigo_comuna"]') , $('#formCliente [name="codigo_provincia"]')[0].value);
    });
    $('#formCliente [name="codigo_provincia"]').live('mousedown mouseover keyup blur',function(){
        if($('#formCliente [name="codigo_region"]').val() === ''){
            $(this).html('');
        }
    });
    
    $('#formCliente [name="codigo_provincia"]').live('change',function(){
        findComunas( $('#formCliente [name="codigo_comuna"]') , this.value);
    });
    $('#formCliente [name="codigo_comuna"]').live('mousedown mouseover keyup blur',function(){
        if($('#formCliente [name="codigo_provincia"]').val() === ''){
            $(this).html('');
        }
    });
    
    function findRegiones(element){
        //$(element).attr('disabled','disabled');//Molesta al hacer tab
        $(element).html('<option value="">Cargando...</option>');
        /*getJSON de JQuery que carga URL regiones?find que retorna el SELECT de las
         * Regiones en formato JSON*/
        $.getJSON( "regiones?find", function( regiones ) {
            $(element).html('');
            if( regiones !== null ){
                options = '<option></option>';
                /*Para cada region, recorrer y ejecutar*/
                $.each(regiones, function() {
                    options += '<option value="'+this.codigoRegion+'">'+this.nombre+'</option>';
                });
                
                $(element).html( options );
                //$(element).removeAttr('disabled');
            }
        });
    }
    
    function findProvincias(element, region){
        //$(element).attr('disabled','disabled');
        $(element).html('<option value="">Cargando...</option>');
        
        $.getJSON( "provincias?find&region="+region, function( provincias ) {
            $(element).html('');
            if( provincias !== null ){
                options = '<option></option>';
                /*Para cada region, recorrer y ejecutar*/
                $.each(provincias, function() {
                    options += '<option value="'+this.codigoProvincia+'">'+this.nombre+'</option>';
                });

                $(element).html( options );
                //$(element).removeAttr('disabled');
            }
        });
    }
    
    function findComunas(element, provincia){
        //$(element).attr('disabled','disabled');
        $(element).html('<option value="">Cargando...</option>');
        
        $.getJSON( "comunas?find&provincia="+provincia, function( comunas ) {
            $(element).html('');
            if( comunas !== null ){
                options = '<option></option>';
                /*Para cada region, recorrer y ejecutar*/
                $.each(comunas, function() {
                    options += '<option value="'+this.codigoComuna+'">'+this.nombre+'</option>';
                });
                
                $(element).html( options );
                //$(element).removeAttr('disabled');
            }
        });
    }
</script>

<fieldset>
    <legend>Agregar Cliente</legend>
    <form id="formCliente" name="formCliente" action="" method="POST" enctype="multipart/form-data">
        <p>
            <label for="cliente_tipo">Tipo de cliente:</label>
            <select name="cliente_tipo" id="cliente_tipo">
                <option value="1">Persona</option>
                <option value="2">Empresa</option>
            </select>
        </p>
        <p>
            <label for="cliente_rut">Rut:</label>
            <input maxlength="15" type="text" name="rut" id="cliente_rut" placeholder="Rut" required=""/>
            <span class="alertRut"></span>
        </p>
        <p>
            <label for="nombres">Nombres:</label>
            <input maxlength="100" type="text" name="nombres" id="nombres" placeholder="Nombres" required=""/>
        </p>
        <p>
            <label for="primer_apellido">Apellido Paterno:</label>
            <input maxlength="100" type="text" name="primer_apellido" id="primer_apellido" placeholder="Primer Apellido"/>
        </p>
        <p>
            <label for="segundo_apellido">Apellido Materno:</label>
            <input maxlength="100" type="text" name="segundo_apellido" id="segundo_apellido" placeholder="Segundo Apellido"/>
        </p>
        <p>
            <label for="direccion">Dirección:</label>
            <input maxlength="100" type="text" name="direccion" id="direccion" placeholder="Dirección" required=""/>
        </p>
        <p>
            <label for="codigo_region">Región:</label>
            <select name="codigo_region" id="codigo_region"></select>
        </p>
        <p>
            <label for="codigo_provincia">Provincia:</label>
            <select name="codigo_provincia" id="codigo_provincia"></select>
        </p>
        <p>
            <label for="codigo_comuna">Comuna:</label>
            <select name="codigo_comuna" id="codigo_comuna"></select>
        </p>
        <p>
            <label for="fono1">Fono1:</label>
            <input maxlength="20" type="text" name="fono1" id="fono1" placeholder="Fono1" required=""/>
        </p>
        <p>
            <label for="fono2">Fono2:</label>
            <input maxlength="20" type="text" name="fono2" id="fono2" placeholder="Fono2" required=""/>
        </p>
        <p>
            <label for="email">E-Mail:</label>
            <input maxlength="100" type="text" name="email" id="email" placeholder="E-Mail" required=""/>
        </p>
        <p>
            <button type="submit">Registrar Cliente</button>
            <button id="btnReset" type="reset">Limpiar Formulario</button>
        </p>
</form>
</fieldset>
