<%-- 
    Document   : consultar-servicio
    Created on : 22-10-2013, 01:40:25 AM
    Author     : jorge
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<script type="text/javascript">
    $(document).ready( function (){
        if(hash === "consultar-servicio"){
            startEntradas();
        }
    });
    
    $('#menu-consultar-servicio > a').live('click',function(){
        startEntradas();
    });
    
    function startEntradas(){
        location.hash = "consultar-servicio";
        findEntradas(0);
    }
    
    function findEntradas(start){
        $('#table-entradas tbody').html('<tr><td colspan="5" style="text-align:center;font-weight:bold;font-size:20px;">Cargando...</td></tr>');
        /*getJSON de JQuery que carga URL listaBodegas que retorna el SELECT de las
         * Bodegas en formato JSON*/
        $.getJSON( "entradas?find&start="+start, function( entradas ) {
            $('#table-entradas tbody').html('');
            /*Para cada contenido de bodega, recorrer y ejecutar*/
            $.each(entradas, function() {
                $('#table-entradas tbody').append('<tr>\
                                                        <td><a href="#ver-entrada">'+this.entradaId+'</a><input value="'+this.entradaId+'" hidden="hidden"></td>\
                                                        <td>'+this.serie+'</td>\
                                                        <td>'+this.modelo+'</td>\
                                                        <td>'+this.comentario+'</td>\
                                                        <td>'+this.accesorios+'</td>\
                                                    </tr>');
            });
        });
    }
    
    $('#table-entradas a').live('click',function(){
        startVerEntrada($(this).parent().find('input').val());
        return false;
    });
    
    
</script>

<div id="div-entradas">
    <h2>Entradas</h2>
    <table id="table-entradas" class="entidades">
        <thead>
            <tr>
                <th><label>Número</label></th>
                <th><label>Serie</label></th>
                <th><label>Modelo</label></th>
                <th><label>Comentario</label></th>
                <th><label>Accesorios</label></th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
