<%-- 
    Document   : tipoEquipos
    Created on : 11-11-2013, 08:23:31 PM
    Author     : manuel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<script type="text/javascript">
    //Sólo ejecutaba la última funcion, no pueden iniciarse múltiples windos.onload
    //window.onload = function(){
    $(document).ready( function (){
        if(hash === "tipo-equipo"){
            startTipoEquipos();
        }
    });
    
    $('#menu-tipoEquipos > a').live('click',function(){
        startTipoEquipos();
    });
    
    actual = 0;
    $('#div-tipoEquipo .controlTabla .tablaSiguiente').live('click',function(){
        if($('#table-tipoEquipos tbody tr').length === 10){
            findTipoEquipo(actual+10);
            actual += 10;
        }
    });
    $('#div-tipoEquipo .controlTabla .tablaAnterior').live('click',function(){
        if(actual-10 > 0){
            findTipoEquipo(actual-10);
            actual -= 10;
        }else{
            findTipoEquipo(0);
            actual = 0;
        }
    });
    $('#div-tipoEquipo .controlTabla .tablaInicial').live('click',function(){
        findTipoEquipo(0);
        actual = 0;
    });
    
    $('#div-tipoEquipo #table-tipoEquipos .eliminar').live('click',function(){
        delTipoEquipo( $(this).parent().parent().find('td').find('input').val() );
    });
    
    $('#div-tipoEquipo #table-tipoEquipos .editar').live('click',function(){
        editTipoEquipo( $(this).parent().parent().find('td').find('input').val() );
    });
    
    
    function startTipoEquipos(){
        location.hash = "tipo-equipo";
        findTipoEquipos(0);
        actual = 0;
    }
    
    function findTipoEquipos(start){
        $('#table-tipoEquipos tbody').html('<tr><td colspan="9" style="text-align:center;font-weight:bold;font-size:20px;">Cargando...</td></tr>');
        /*getJSON de JQuery que carga URL listaBodegas que retorna el SELECT de las
         * Bodegas en formato JSON*/
        $.getJSON( "tipoEquipo?find&start="+start, function( tipoEquipos ) {
            $('#table-tipoEquipos tbody').html('');
            /*Para cada contenido de bodega, recorrer y ejecutar*/
            $.each(tipoEquipos, function() {
                $('#table-tipoEquipos tbody').append('<tr>\
                                                        <td>'+this.nombre+'<input value="'+this.tipoId+'" hidden="hidden">'+'</td>\
                                                        <td>'+this.costoRevision+'</td>\
                                                        <td><button type="button" class="editar">Editar</button></td>\
                                                        <td><button type="button" class="eliminar">Eliminar</button></td>\
                                                    </tr>');
            });
        });
    }
    
    
    function editTipoEquipo(tipoId){
        $.getJSON( "tipoEquipo?encontrar&tipoId="+tipoId, function( respuesta ) {
            if(respuesta){
                $('#formEditarTipoEquipo [name="tipoId"]').val(respuesta.tipoId);
                $('#formEditarTipoEquipo [name="nombre"]').val(respuesta.nombre);
                $('#formEditarTipoEquipo [name="costo"]').val(respuesta.costoRevision);
            }else{
                alert("Error!\nNo se pudo encontrar.");
            }
        });
        location.hash = "editar-tipo-equipo";
    }
    
    function delTipoEquipo(tipoId){
        $.getJSON( "tipoEquipo?delete&tipoId="+tipoId, function( respuesta ) {
            if(respuesta['noError'] === true){
                alert("Eliminado!");
                startTipoEquipos();
            }else{
                alert("Error!\nNo se pudo eliminar.");
            }
        });
    }
</script>

<div id="div-tipoEquipo">
    <h2>Tipos de Equipos</h2>
    <div class="controlTabla">
        <button class="tablaAnterior" type="button">Anterior</button>
        <button class="tablaInicial" type="button">Inicio</button>
        <button class="tablaSiguiente" type="button">Siguiente</button>
    </div>
    <table id="table-tipoEquipos" class="entidades">
        <thead>
            <tr>
                <th><label>Nombre</label></th>
                <th><label>Costo Revision</label></th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>