<%-- 
    Document   : editar-estado
    Created on : 10-11-2013, 09:59:11 AM
    Author     : ale
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<script type="text/javascript">
    $(document).ready(function() {
        $('#formEditarEstado').submit(function(){

            $.getJSON( "estados?editar&json="+JSON.parse(JSON.stringify(JSON.stringify({
                actualizacionId:$('#formEditarEstado [name="actualizacionId"]').val(),
                nombre:$('#formEditarEstado [name="nombre"]').val(),
                actualizacionTipo:3
            }))),
                function( respuesta ) {
                    if(respuesta){
                        if(respuesta['noError'] === true){
                            alert("Editado correctamente");
                            $('#formEditarEstado')[0].reset();
                            startEstados();
                        }else{
                            alert("Error!\nPosiblemente ya existe.");
                        }
                    }
            });
            
            return false;
        });
    });
    
</script>

<fieldset>
    <legend>Editar Estado</legend>
    <form id="formEditarEstado" name="formEditarEstado" action="" method="POST" enctype="multipart/form-data">
        
        <p>
            <label for="nombre">Nombre:</label>
            <input maxlength="100" type="text" name="actualizacionId" id="actualizacionId" placeholder="Id" hidden="hidden"/>
            <input maxlength="100" type="text" name="nombre" id="nombre" placeholder="Nombre" required=""/>
            <input maxlength="100" type="text" name="actualizacionTipo" id="actualizacionTipo" placeholder="Id" hidden="hidden"/>
        </p>
        <p>
            <button type="submit">Editar Estado</button>
        </p>
</form>
</fieldset>
