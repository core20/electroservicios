<%-- 
    Document   : ver-entrada
    Created on : 20-11-2013, 12:08:42 PM
    Author     : naxo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script>
    function startVerEntrada(servicio){
        location.hash = "ver-entrada";
        $.getJSON( "entradas?encontrar&id="+servicio, function( entrada ) {
            $('#formVerEntrada #servicio').val(entrada.entradaId);
            $('#formVerEntrada #serie').val(entrada.serie);
            $('#formVerEntrada #modelo').val(entrada.modelo);
        });
        
        $.getJSON( "entradas?historial&id="+servicio, function( historial ) {
            $('#formVerEntrada #table-verentradas tbody').html('');
            /*Para cada contenido de bodega, recorrer y ejecutar*/
            $.each(historial, function() {
                $('#formVerEntrada #table-verentradas tbody').append('<tr>\
                                                        <td>'+this.detalle+'\
                                                        <td>'+this.fecha+'</td>\
                                                        <td>'+this.detalle+'\
                                                        <td>'+this.valor+'</td>\
                                                    </tr>');
            });
        });
    }
        
    $('#formVerEntrada .volverEntrada').live('click',function(){
        startEntradas();
    });
    
    $('#formVerEntrada .agregarEstado').live('click',function(){
        startHistorial($('#formVerEntrada #servicio').val());
    });
    
</script>
<fieldset>
    <legend>Ver Entrada</legend>
    <form id="formVerEntrada" name="formVerEntrada" action="" method="POST" enctype="multipart/form-data">
        <p>
            <label for="servicio">Número de Servicio:</label>
            <input style="color: #3C3C3C;" maxlength="100" type="text" name="servicio" id="servicio" value="" placeholder="Servicio" disabled="disabled"/>
        </p>
        <p>
            <label for="serie">Número de Serie:</label>
            <input style="color: #3C3C3C;" maxlength="100" type="text" name="serie" id="serie" placeholder="Serie" required="" disabled="disabled"/>
        </p>
        <p>
            <label for="modelo">Modelo:</label>
            <input style="color: #3C3C3C;" maxlength="100" type="text" name="modelo" id="modelo" placeholder="Modelo" required="" disabled="disabled"/>
        </p>
        
        <hr>
        <p>
            <label>Historial</label>
        </p>
        <table id="table-verentradas" class="entidades">
            <thead>
                <tr>
                    <th><label>Intervención</label></th>
                    <th><label>Fecha</label></th>
                    <th><label>Detalle</label></th>
                    <th><label>Valor</label></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <p>
            <button type="button" class="agregarEstado">Agregar Estado</button>
        </p>
        <hr>
        <table>
            <tr>
                <td>
                    <p><label for="total">Total:</label>
                </td>
                <td>
                    <p><input style="color: #3C3C3C;" maxlength="100" type="text" name="total" id="total" value="" placeholder="Total"/></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><label for="abono">Abono:</label></p>
                </td>
                <td>
                    <p><input style="color: #3C3C3C;" maxlength="100" type="text" name="abono" id="abono" value="" placeholder="Abono"/></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><label for="apagar">A Pagar:</label></p>
                </td>
                <td>
                    <p><input style="color: #3C3C3C;" maxlength="100" type="text" name="apagar" id="apagar" value="" placeholder="A Pagar"/></p>
                </td>
            </tr>
        </table>
        
        <br />
        <p>
            <button type="button" class="volverEntrada"><- Volver a entradas</button>
            <!--<button type="submit">Dar Salida</button>-->
        </p>
    </form>
</fieldset>
