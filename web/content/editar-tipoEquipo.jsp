<%-- 
    Document   : editar-marca
    Created on : 10-11-2013, 09:59:11 AM
    Author     : ale
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<script type="text/javascript">
    $(document).ready(function() {
        $('#formEditarMarca').submit(function(){

            $.getJSON( "marcas?editar&json="+JSON.parse(JSON.stringify(JSON.stringify({
                marcaId:$('#formEditarMarca [name="marcaId"]').val(),
                nombre:$('#formEditarMarca [name="nombre"]').val()
            }))),
                function( respuesta ) {
                    if(respuesta){
                        if(respuesta['noError'] === true){
                            alert("Editado correctamente");
                            $('#formEditarMarca')[0].reset();
                            startMarcas();
                        }else{
                            alert("Error!\nPosiblemente ya existe.");
                        }
                    }
            });
            
            return false;
        });
    });
    
</script>

<fieldset>
    <legend>Editar Marca</legend>
    <form id="formEditarMarca" name="formEditarMarca" action="" method="POST" enctype="multipart/form-data">
        
        <p>
            <label for="nombre">Nombre:</label>
            <input maxlength="100" type="text" name="marcaId" id="marcaId" placeholder="Id Marca" hidden="hidden"/>
            <input maxlength="100" type="text" name="nombre" id="nombre" placeholder="Nombre" required=""/>
        </p>
        <p>
            <button type="submit">Editar Marca</button>
        </p>
</form>
</fieldset>
