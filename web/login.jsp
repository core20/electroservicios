<%@page import="Controlador.UsuarioJpaController"%>
<%@page import="javax.persistence.EntityManager"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="Entidades.Usuario"%>
<%
    //HttpSession session ya est� creado por defecto
    
    if(request.getParameter("logout") != null){
        session.invalidate();
        response.sendRedirect("index.jsp#logout");      //Invalida sesion
    }
    
    /*Si se envia formulario usuario se inicia session*/
    if(request.getParameter("usuario") != null){
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("electroserviciosPU");
        EntityManager em = emf.createEntityManager();
        
        UsuarioJpaController ujc = new UsuarioJpaController(null, emf);
        Usuario usuario = ujc.findUsuarioByName(request.getParameter("nombre-usuario"));
        
        if( usuario != null && usuario.getPassword().equals(request.getParameter("password").toString()) ){
            session.setAttribute("session-tipo", "usuario");
            session.setAttribute("usuario", usuario);

            response.sendRedirect("panel.jsp");
        }else{
            response.sendRedirect("index.jsp#datos-erroneos");
        }
    }
%>
