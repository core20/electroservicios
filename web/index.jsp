<%--<%
/*Validar que session corresponda*/
if(session.getAttribute("session-tipo") != "admin"){
    response.sendRedirect("../index.jsp");
}
%>--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ElectroServicios</title>
        <link href="style/index.css" type="text/css" rel="stylesheet">
        <link rel="icon" type="image/png" href="images/logo.png" />
        <!--<script type="text/javascript">
            var hash = location.hash.substr(1);
            window.onload = function(){
                if(hash === "datos-erroneos"){
                    alert("Datos incorrectos");
                }
                
                if(hash === "logout"){
                    alert("Sesión cerrada!");
                }
            };
        </script>-->
        <style type="text/css">
            #alerta p {
                display: none;
                position: absolute;
                text-align: center;
                font-size: 16px;
                font-weight: bold;
                width: 100%;
            }
            #alerta p:target {
                display: block;
            }
            #alerta p span {
                padding: 10px 20px;
                background: #EEE;
                border-radius: 4px;
                border: 5px solid #C88;
            }
            
            @keyframes animacionAlerta{
                0% {opacity:.1;}
                25% {padding:20px 40px; font-size:20px;;}
                50% {padding:5px 10px; font-size:14px;}
                70% {padding:15px 30px; font-size:18px;}
                85% {padding:8px 16px; font-size:15px;}
                to {opacity:1;}
            }
            #alerta p:target span {
                animation: animacionAlerta 1s 1 forwards;
            }
            
            @keyframes aparicionDeIzquierdaADerecha{
                0% {opacity:.1; transform:translateX(-50px);}
                to {opacity:1; transform:translateX(0);}
            }
            #iniciar-sesion fieldset {
                animation: aparicionDeIzquierdaADerecha 0.5s 1 forwards;
            }
        </style>
    </head>
    <body>
        <div id="barra">
            <a href="index.jsp">
                <img id="logo" src="images/logo.png">
                <span>ElectroServicios</span>
            </a>
        </div>
        <div id="alerta">
            <p id="datos-erroneos"><span>Datos incorrectos</span></p>
            <p id="logout"><span>Sesión cerrada!</span></p>
        </div>
        <div id="content">
            <div id="iniciar-sesion">
                <fieldset>
                    <legend>Iniciar sesión</legend>
                    <form id="form-login" name="form-login" action="login.jsp" method="POST">
                        <p>
                            <label for="nombre-usuario">Nombre de usuario:</label>
                            <input type="text" value="" name="nombre-usuario" id="nombre-usuario" placeholder="Usuario" required="">
                        </p>
                        <p>
                            <label for="password">Contraseña</label>
                            <input type="password" value="" name="password" id="password" placeholder="Contraseña" required="">
                        </p>
                        <p>
                            <input type="submit" value="Entrar" name="usuario" id="usuario">
                        </p>
                    </form>
                </fieldset>
            </div>
        </div>
    </body>
</html>
