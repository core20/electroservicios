/* CLIENTE */

INSERT INTO cliente VALUES('17446561','4','Jorge Osvaldo','Arias','Leal','Thiers 442','442314','98824999','jorge@hotmail.com','2','4101');
INSERT INTO cliente VALUES('17982585','6','Adrian Ignacio','Miranda','Contreras','Prat 628','316208','88822142','imiranda@hotmail.com','1','14101');
INSERT INTO cliente VALUES('13077584','5','Juan Leon','Reyes','Albornoz','Balmaceda 122','333222','11133241','juan@hotmail.com','1','5702');
INSERT INTO cliente VALUES('6059197','0','Patricio Andres','Barrientos','Lagos','Lautaro 344','334222','99923445','patricio@hotmail.com','1','7102');
INSERT INTO cliente VALUES('21008754','0','Paola Fernanda','Muñoz','Rivas','Balmaceda 143','543221','77833453','paola@hotmail.com','2','14101');
INSERT INTO cliente VALUES('17946099','8','Nicolas Patricio','Miranda','Contreras','Prat 628','316208','99533211','nicolas@hotmail.com','1','14101');
INSERT INTO cliente VALUES('16956443','4','Luis Alberto','Flores','Gallardo','Gabriela Mistral 1432','444553','88533221','luis@hotmail.com','2','14108');
INSERT INTO cliente VALUES('7899537','8','Karla Javiera','Conejeros','Ferrada','Miraflores','642112','78832113','alejandra@hotmail.com','1','10402');
INSERT INTO cliente VALUES('12090124','9','Mayra Dominique','Maldonado','Gonzales','Mendez 884','73430','96702182','mayra@hotmail.com','1','9208');
INSERT INTO cliente VALUES('14673789','7','Ariel Esteban','Garrido','Zapata','Bulnes 223','334','88321133','ariel@hotmail.com','2','9101');


/* EMPLEADO */

INSERT INTO empleado VALUES('17446561','4','Jorge Osvaldo','Arias','Leal');
INSERT INTO empleado VALUES('18118150','8','Alejandra Isabel','Quiñilef','Inostroza');
INSERT INTO empleado VALUES('16986061','0','Manuel Octavio','Vidal','Jaque');
INSERT INTO empleado VALUES('6887830','6','Erik Joaquin','Saez','Leal');
INSERT INTO empleado VALUES('6561383','2','Mauricio Andres','Israel','Lopez');
INSERT INTO empleado VALUES('19397366','3','Carlos Alberto','Urra','Valdebenito');


/* USUARIO */

INSERT INTO usuario VALUES (null, 'jarias', '1234', '1', '2', '17446561');
INSERT INTO usuario VALUES (null, 'quinilef', '1234', '1', '1', '18118150');
