<%-- 
    Document   : panel2
    Created on : 18-10-2013, 12:22:56 PM
    Author     : jorge
--%>

<%@ include file="WEB-INF/session.jsp" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ElectroServicios</title>
        <link href="style/style.css" type="text/css" rel="stylesheet">
        <link rel="shortcut icon" type="image/png" href="images/logo.png" />
        <script src="javascript/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            //Sólo ejecutaba la última funcion, no pueden iniciarse múltiples windos.onload
            var hash = location.hash.substr(1);
            //window.onload = function(){
            $(document).ready( function (){
                if(hash === ""){
                    location.hash = "inicio";
                }
                noScroll();
            });
            
            $('#main-menu a').live('click',function(){
                noScroll();
            });
            $('.noTargetScroll').live('click',function(){
                noScroll();
            });
            
            /*Anular Scroll al cambiar :target*/
            function noScroll(){
                if (window.pageYOffset !== null){
                    oldY = window.pageYOffset+'';
                }
                if (document.body.scrollWidth !== null){
                    if (document.body.scrollTop){
                    oldY = document.body.scrollTop;
                }
                    oldY = document.documentElement.scrollTop;
                }
                
                if (window.pageXOffset !== null){
                    oldX = window.pageXOffset+'';
                }
                if (document.body.scrollTop !== null){
                    if (document.body.scrollWidth){
                    oldX = document.body.scrollWidth;
                }
                    oldX = document.documentElement.scrollWidth;
                }
                
                setTimeout('window.scroll(oldX,oldY)',10);//Cambiado para dar mas tiempo
                //setTimeout('window.scroll(oldX,oldY)',10);//Cambiar scroll luego de 0.01 segundos, cuando ya se haya cambiado Scroll auto
                setTimeout('window.scroll(oldX,oldY)',100);//Agregado para repetir en mas tiempo
            }
        </script>
    </head>
    <body>
        <h3 id="nombreusuario" style="line-height:40px;float: right;margin-right: 50px; color: #FFF;">Usuario: <%= usuario.getNombreUsuario() %> (<%= (usuario.getUsuarioTipo()==1)?"Administrador":((usuario.getUsuarioTipo()==2)?"Vendedor":(usuario.getUsuarioTipo()==3)?"Tecnico":"unknow") %>) <a href="login.jsp?logout" style="color: yellow;">Salir</a></h3>
        <ul id="main-menu">
            <li id="menu-inicio"><a href="#inicio"><img id="logo" src="images/logo.png">Inicio</a></li
            ><li id="menu-dar-entrada"><a href="#dar-entrada">Dar Entrada</a></li
            ><li id="menu-consultar-servicio"><a href="#consultar-servicio">Consultar Servicio</a></li
            ><li id="menu-clientes"><a href="#clientes">Clientes</a>
                <ul>
                    <li><a href="#agregar-cliente">Agregar Nuevo</a></li>
                </ul>
            </li
            ><li id="menu-empleados"><a href="#empleados">Empleados</a>
                <ul>
                    <li><a href="#agregar-empleado">Agregar Nuevo</a></li>
                </ul>
            </li
            ><li id="menu-marcas"><a href="#marcas">Marcas</a>
                <ul>
                    <li><a href="#agregar-marca">Agregar Nuevo</a></li>
                </ul>
            </li
            ><li id="menu-tipoEquipos"><a href="#tipo-equipo">Tipo Equipos</a>
                <ul>
                    <li><a href="#agregar-tipo-equipo">Agregar Nuevo</a></li>
                </ul>
            </li
            ><li id="menu-estados"><a href="#estados">Estados</a>
                <ul>
                    <li><a href="#agregar-estado">Agregar Nuevo</a></li>
                </ul>
            </li
            >
        </ul>
        <ul id="main-conent">
            <li id="inicio">
                <div>
                    <%@ include file="content/inicio.jsp" %>
                </div>
            </li>
            <li id="dar-entrada">
                <div>
                    <%@ include file="content/dar-entrada.jsp" %>
                </div>
            </li>
            <li id="consultar-servicio">
                <div>
                    <%@ include file="content/consultar-servicio.jsp" %>
                </div>
            </li>
            <li id="clientes">
                <div>
                    <%@ include file="content/clientes.jsp" %>
                </div>
            </li>
            <li id="agregar-cliente">
                <div>
                    <%@ include file="content/agregar-cliente.jsp" %>
                </div>
            </li>
            <li id="editar-clientes">
                <div>
                    <%@ include file="content/editar-clientes.jsp" %>
                </div>
            </li>
            <li id="empleados">
                <div>
                    <%@ include file="content/empleados.jsp" %>
                </div>
            </li>
            <li id="agregar-empleado">
                <div>
                    <%@ include file="content/agregar-empleado.jsp" %>
                </div>
            </li>
            <li id="editar-empleado">
                <div>
                    <%@ include file="content/editar-empleado.jsp" %>
                </div>
            </li>
            <li id="marcas">
                <div>
                    <%@ include file="content/marcas.jsp" %>
                </div>
            </li>
             <li id="agregar-marca">
                <div>
                    <%@ include file="content/agregar-marca.jsp" %>
                </div>
            </li>
            <li id="editar-marca">
                <div>
                    <%@ include file="content/editar-marca.jsp" %>
                </div>
            </li>
            <li id="tipo-equipo">
                <div>
                    <%@ include file="content/tipo-equipo.jsp" %>
                </div>
            </li>
             <li id="agregar-tipo-equipo">
                <div>
                    <%@ include file="content/agregar-tipo-equipo.jsp" %>
                </div>
            </li>
            <li id="editar-tipo-equipo">
                <div>
                    <%@ include file="content/editar-tipo-equipo.jsp" %>
                </div>
            </li>
            <li id="agregar-estado">
                <div>
                    <%@ include file="content/agregar-estado.jsp" %>
                </div>
            </li>
            <li id="estados">
                <div>
                    <%@ include file="content/estados.jsp" %>
                </div>
            </li>
            <li id="editar-estado">
                <div>
                    <%@ include file="content/editar-estado.jsp" %>
                </div>
            </li>
            <li id="ver-entrada">
                <div>
                    <%@ include file="content/ver-entrada.jsp" %>
                </div>
            </li>
            <li id="agregar-historial">
                <div>
                    <%@ include file="content/agregar-historial.jsp" %>
                </div>
            </li>
            <li id="usuarios">
                <div>
                </div>
            </li>
        </ul>
    </body>
</html>
